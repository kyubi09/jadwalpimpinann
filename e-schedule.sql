-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 27 Jul 2019 pada 11.25
-- Versi server: 10.3.16-MariaDB
-- Versi PHP: 7.3.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `e-schedule`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `jadwal`
--

CREATE TABLE `jadwal` (
  `id` int(11) NOT NULL,
  `tanggal` date DEFAULT NULL,
  `waktu` varchar(20) DEFAULT NULL,
  `kegiatan` varchar(128) DEFAULT NULL,
  `tempat` varchar(128) DEFAULT NULL,
  `materi` text DEFAULT NULL,
  `kabadan` varchar(5) DEFAULT NULL,
  `ses` varchar(5) DEFAULT NULL,
  `lainses` text DEFAULT NULL,
  `tekpim` varchar(5) DEFAULT NULL,
  `fungham` varchar(5) DEFAULT NULL,
  `penkom` varchar(5) DEFAULT NULL,
  `poltekip` varchar(5) DEFAULT NULL,
  `poltekim` varchar(5) DEFAULT NULL,
  `cp` varchar(32) DEFAULT NULL,
  `dresscode` varchar(64) DEFAULT NULL,
  `kategori` varchar(64) DEFAULT NULL,
  `keterangan` text DEFAULT NULL,
  `disposisi` text DEFAULT NULL,
  `laporan` text DEFAULT NULL,
  `tindaklanjut` text DEFAULT NULL,
  `dibuat` varchar(20) DEFAULT NULL,
  `lastupdate` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `lastedit` varchar(20) DEFAULT NULL,
  `hadir` varchar(30) DEFAULT NULL,
  `perintahkabadan` int(11) NOT NULL,
  `file` varchar(64) DEFAULT NULL,
  `lainfungham` text DEFAULT NULL,
  `lainpenkom` text DEFAULT NULL,
  `laintekpim` text DEFAULT NULL,
  `lainpoltekip` text DEFAULT NULL,
  `lainpoltekim` text DEFAULT NULL,
  `kode` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `jadwal`
--

INSERT INTO `jadwal` (`id`, `tanggal`, `waktu`, `kegiatan`, `tempat`, `materi`, `kabadan`, `ses`, `lainses`, `tekpim`, `fungham`, `penkom`, `poltekip`, `poltekim`, `cp`, `dresscode`, `kategori`, `keterangan`, `disposisi`, `laporan`, `tindaklanjut`, `dibuat`, `lastupdate`, `lastedit`, `hadir`, `perintahkabadan`, `file`, `lainfungham`, `lainpenkom`, `laintekpim`, `lainpoltekip`, `lainpoltekim`, `kode`) VALUES
(41, '2019-07-22', '10.00', 'Menguji Penilaian Kompetensi bagi Pejabat Pengawas', 'Ruang Assessment', '', NULL, '10', '', NULL, NULL, NULL, NULL, NULL, 'PENKOM', '', '', '', NULL, NULL, NULL, 'tuses', '2019-07-26 06:32:42', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, ''),
(42, '2019-07-22', '13.00', 'Rapat Tindak Lanjut Penyelesaian Sengketa Aset Kemenkumham di Tangerang', 'Pendopo Gubernur Banten', '', NULL, '1', '', NULL, NULL, NULL, NULL, NULL, '', '', '', '', NULL, NULL, NULL, 'tuses', '2019-07-24 07:39:25', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, ''),
(43, '2019-07-23', '09.00', 'Rapat Kelulusan Tes Kesamaptaan Penerimaan Sekolah Kedinasan Poltekip dan Poltekim 2019', 'RR. Lt.3 Setjen', '', NULL, '1', '', NULL, NULL, NULL, NULL, NULL, 'BIROWAI', '', '', '', NULL, NULL, NULL, 'tuses', '2019-07-26 06:28:22', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, ''),
(44, '2019-07-23', '08.00', 'Best Practice Sharing Forum Manajemen resiko', 'Hotel JS Luwansa (Ruang Chil Bar Lounge) Jaksel', '', NULL, '1', '', NULL, NULL, NULL, NULL, NULL, '', '', '', '', NULL, NULL, NULL, 'tuses', '2019-07-26 06:28:07', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, ''),
(45, '2019-07-23', '11:00 s/d 14:00', 'Pembahasan Kesepakatan Bersama antara Kemenkumham dan Pemda Tangerang', 'Hotel Aryaduta Lippo Karawaci', 'Pe', NULL, '1', '', NULL, NULL, NULL, NULL, NULL, 'BIRO BMN', '', '', '', NULL, NULL, NULL, 'tuses', '2019-07-24 07:45:13', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, ''),
(46, '2019-07-24', '14.00 s/d 16.00', 'Membuka & Narasumber FGD Penyusunan Modul Pelatihan Fungsional Pemeriksa Keimigrasian', 'Hotel The Alana, Jalan Ir. Haji Djuanda No.76 Sentul, Bogor', '', '1', NULL, '', NULL, NULL, NULL, NULL, NULL, '', '', '', '', NULL, NULL, NULL, 'tukabadan', '2019-07-24 08:21:13', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, ''),
(47, '2019-07-24', '17.00', 'Menghadiri Rapat Pimpinan bersama Menkumham dalam rangka membahas Isu-isu Aktual Kemenkumham', 'Ruang Rapat Menteri Hukum dan HAM Lt. 5 Gd. Ex Sentra Mulia Jl. H.R. Rasuna Said Kav. 6-7, Kuningan, Jakarta Selatan', '', '1', NULL, '', NULL, NULL, NULL, NULL, NULL, '', '', '', '', NULL, NULL, NULL, 'tukabadan', '2019-07-24 08:21:56', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, ''),
(48, '2019-07-25', '13.00 s/d 14.00', 'Memberikan sambutan & menutup Diklat Terpadu SPPA bagi apgakum dan instansi terkait Angkatan XLII', 'GH', '', '10', NULL, '', NULL, NULL, NULL, NULL, NULL, '', '', '', '', 'Jadwalkan 24/07/2019', NULL, NULL, 'tukabadan', '2019-07-25 07:27:12', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, ''),
(49, '2019-07-26', '09.00', 'Memberikan sambutan & menutup Diklat Fungsional PK Angkatan X TA 2019', 'GH', '', '1', NULL, '', NULL, NULL, NULL, NULL, NULL, '', '', '', '', NULL, NULL, NULL, 'tukabadan', '2019-07-24 08:24:51', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, ''),
(50, '2019-07-26', '14.00 s/d Selesai', 'Membuka & memberikan sambutan pada kegiatan pembukaan Tugas Belajar (Crash Program) POLTEKIP', 'Auditorium BPSDM Hukum dan HAM', '', '1', NULL, '', NULL, NULL, NULL, NULL, NULL, '', 'PDH II', '', '', 'Jadwalkan 23/07/2019', NULL, NULL, 'tukabadan', '2019-07-24 08:26:12', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, ''),
(51, '2019-07-28', '05.30 s/d 12.30', 'Family Gathering Golf (Perpisahan Bpk. Mardjoeki)', 'Matoa National Golf Course', '', '10', '1', '', NULL, NULL, NULL, NULL, NULL, '', '', '', '', NULL, NULL, NULL, 'tukabadan', '2019-07-26 10:09:26', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, ''),
(52, '2019-07-22', '07.30 s/d Selesai', 'Membuka & Penceramah dalam Pelatihan Bendahara Pengeluaran yang diselenggarakan oleh Badiklat Jateng', 'HOM Premiere Timoho Kota Yogyakarta Jl. Ipda Tut Harsono No.22, Muja Muju, Kec. Umbulharjo, Kota Yogyakarta, DIY', '', '1', NULL, '', NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', NULL, NULL, 'tukabadan', '2019-07-25 07:02:33', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, ''),
(53, '0000-00-00', '', '', '', '', NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, '', '', '', '', NULL, NULL, NULL, 'tuses', '2019-07-24 11:31:07', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, ''),
(54, '2019-07-25', '08.00', 'Menguji Assessment Center bagi Pejabat Eselon IV dan V di Lingkungan Kanwil Kemenkumham Banten', 'Kanwil Kemenkumham Banten', '', NULL, '1', '', NULL, NULL, NULL, NULL, NULL, '', '', '', 'tidak menginap', NULL, NULL, NULL, 'tuses', '2019-07-26 06:34:23', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, ''),
(55, '0000-00-00', '', '', '', '', NULL, NULL, '', NULL, NULL, NULL, NULL, '1', '', '', '', '', NULL, NULL, NULL, 'tupoltekim', '2019-07-25 06:19:03', NULL, NULL, 0, 'efd3b261a3b9e457505e5bb430b8181d.docx', NULL, NULL, NULL, NULL, NULL, ''),
(56, '0000-00-00', '', '', '', '', NULL, NULL, '', NULL, NULL, NULL, NULL, '1', '', '', '', '', NULL, NULL, NULL, 'tupoltekim', '2019-07-25 06:19:27', NULL, NULL, 0, '8bb7c95a8b0c4d9fa415560cf7a25149.doc', NULL, NULL, NULL, NULL, NULL, ''),
(57, '2019-07-26', '', '', '', '', NULL, NULL, '', NULL, NULL, NULL, '1', '1', '', '', '', '', NULL, NULL, NULL, 'tupoltekip', '2019-07-25 07:29:26', NULL, NULL, 0, '602d6ea0174f8a1af3974e8dfdd85630.doc', NULL, NULL, NULL, NULL, NULL, ''),
(58, '0000-00-00', '', 'dengan file', '', '', NULL, NULL, '', NULL, NULL, NULL, '1', '10', '', '', '', '', NULL, NULL, NULL, 'tupoltekip', '2019-07-25 07:26:12', NULL, NULL, 0, 'eac6a7fb80f73c08ae8854dc8e487421.doc', NULL, NULL, NULL, NULL, NULL, ''),
(60, '0000-00-00', '', '', 'tanpa file', '', NULL, NULL, '', NULL, NULL, NULL, '1', '1', '', '', '', '', NULL, NULL, NULL, 'tupoltekip', '2019-07-25 07:19:52', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, ''),
(61, '2019-07-26', '08.00', 'Penutupan Diklat Fungsional PK Bapas Angkatan X', 'GH BPSDM', '', '1', '1', '', '1', '1', '1', NULL, NULL, '', '', '', '', NULL, NULL, NULL, 'tufungham', '2019-07-25 06:35:29', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, ''),
(62, '2019-07-26', '10.00', 'Tester', 'tes1', '', '1', NULL, '', NULL, NULL, NULL, NULL, '1', '', '', '', '', NULL, NULL, NULL, 'tukabadan', '2019-07-25 07:31:38', NULL, NULL, 1, 'a5d1c66125c4271a05c2a05fe8bc178b.doc', NULL, NULL, NULL, NULL, NULL, ''),
(63, '2019-07-26', '16.30', 'Pulang', 'Rumah', '', NULL, NULL, '', NULL, NULL, NULL, NULL, '4', '', '', '', '', NULL, NULL, NULL, 'tupoltekim', '2019-07-25 07:31:00', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, ''),
(64, '2019-07-01', '10.00', 'rapat', 'rapat', 'memimpin', NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, '', '', '', '', NULL, NULL, NULL, 'tupoltekim', '2019-07-25 07:00:15', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, ''),
(65, '2019-07-22', '', 'Benchmarking Pendidikan dan Pelatihan Kepemimpinan Tingkat III', 'Surabaya', 'Benchmarking Pendidikan dan Pelatihan Kepemimpinan Tingkat III', NULL, NULL, '', '1', NULL, NULL, NULL, NULL, '', '', '', '', NULL, NULL, NULL, 'tutekpim', '2019-07-25 07:30:02', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, ''),
(66, '2019-07-23', '', 'Benchmarking Pendidikan dan Pelatihan Kepemimpinan Tingkat III', 'Surabaya', 'Benchmarking Pendidikan dan Pelatihan Kepemimpinan Tingkat III', NULL, NULL, '', '1', NULL, NULL, NULL, NULL, '', '', '', '', NULL, NULL, NULL, 'tutekpim', '2019-07-25 07:18:52', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, ''),
(67, '2019-07-26', '14.00', 'Pembukaan Crash Program Poltekip', 'Auditorium', '', NULL, '1', '', NULL, NULL, NULL, NULL, NULL, '', '', '', '', NULL, NULL, NULL, 'tuses', '2019-07-26 06:38:09', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, ''),
(68, '2019-07-29', '08.00', 'Menguji Seminar Laboratorium Kepemimpinan bagi Peserta Diklat PIM Tk III Angk 58', 'R Kelas BPSDM', '', NULL, '1', '', NULL, NULL, NULL, NULL, NULL, '', '', '', '', NULL, NULL, NULL, 'tuses', '2019-07-26 06:40:02', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, ''),
(69, '2019-07-30', '08.00', 'Menguji Seminar Laboratorium Kepemimpinan bagi Peserta Diklat PIM Tk IV Angk 189 ', 'R Kelas BPSDM', '', NULL, '1', '', NULL, NULL, NULL, NULL, NULL, '', '', '', '', NULL, NULL, NULL, 'tuses', '2019-07-26 06:40:43', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, ''),
(70, '2019-07-31', '08.00', 'Menguji Seminar RPP Kepemimpinan bagi Peserta Diklat PIM Tk III Angk 59', 'R Kelas BPSDM', '', NULL, '1', '', NULL, NULL, NULL, NULL, NULL, '', '', '', '', NULL, NULL, NULL, 'tuses', '2019-07-26 06:41:15', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, ''),
(71, '2019-08-01', '15.00', 'Penutupan Trailor Made Training Stuned Nuffic Neso dalam Program Pengembangan Penyuluh Hukum', 'Savero Hotel Depok', '', NULL, '1', '', NULL, NULL, NULL, NULL, NULL, '', '', '', '', NULL, NULL, NULL, 'tuses', '2019-07-26 06:42:03', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, ''),
(72, '2019-07-29', '08.00', 'Pembukaan Diklat PK Bapas  XI', 'Guest House BPSDM Hukum dan HAM', '', '1', '1', '', '1', '1', '1', NULL, NULL, '', '', '', '', NULL, NULL, NULL, 'tufungham', '2019-07-26 07:01:09', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, ''),
(73, '2019-07-29', '10.00', 'Rapat Evaluasi Akhir Peserta Diklat Penyuluh Hukum', 'Ruang Rapat Gd Pusat', '', NULL, NULL, '', NULL, '1', NULL, NULL, NULL, '', '', '', '', NULL, NULL, NULL, 'tufungham', '2019-07-26 07:03:16', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, ''),
(74, '2019-07-29', '16.00', 'Penutupan Diklat Fungsional Penyuluh Hukum', 'Guest House BPSDM Hukum dan HAM', '', '1', '1', '', '1', '1', '1', NULL, NULL, '', '', '', '', NULL, NULL, NULL, 'tufungham', '2019-07-26 07:04:12', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, ''),
(75, '2019-08-03', '10.00', 'Pernikahan Rinaldi dna Utari', 'Jl Tengki I Rt 03/810 Blok Tengki, Kel Meruyung, Limo, Depok', '', NULL, '1', '', NULL, NULL, NULL, NULL, NULL, '', '', '', 'Putra Pak Rosidi Pensiunan BPSDM', NULL, NULL, NULL, 'tuses', '2019-07-26 10:08:38', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, ''),
(76, '2019-07-29', '08.00', 'Pembukaan Diklat Fungsional Calon Pejabat Fungsional Pembimbing Kemasyarakatan Angkatan XI', 'GH', '', NULL, '1', '', NULL, NULL, NULL, NULL, NULL, '', '', '', '', NULL, NULL, NULL, 'tuses', '2019-07-26 10:15:16', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(40) NOT NULL,
  `password` varchar(35) NOT NULL,
  `userkode` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `userkode`) VALUES
(1, 'tukabadan', 'e5bb0b4b7af72d02e07bfb4926d5a2d6', 1),
(2, 'tuses', '1768612a7dffa6321dc15332a83296ab', 2),
(3, 'tufungham', 'e5bb0b4b7af72d02e07bfb4926d5a2d6', 3),
(4, 'tupenkom', 'e5bb0b4b7af72d02e07bfb4926d5a2d6', 3),
(5, 'tutekpim', 'e5bb0b4b7af72d02e07bfb4926d5a2d6', 3),
(6, 'tupoltekip', 'e5bb0b4b7af72d02e07bfb4926d5a2d6', 3),
(7, 'tupoltekim', 'e5bb0b4b7af72d02e07bfb4926d5a2d6', 3),
(8, 'kabagumum', 'e5bb0b4b7af72d02e07bfb4926d5a2d6', 5),
(9, 'kasubtu', 'e5bb0b4b7af72d02e07bfb4926d5a2d6', 4),
(10, 'protokol', 'e5bb0b4b7af72d02e07bfb4926d5a2d6', 5);

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `jadwal`
--
ALTER TABLE `jadwal`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `jadwal`
--
ALTER TABLE `jadwal`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=77;

--
-- AUTO_INCREMENT untuk tabel `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
