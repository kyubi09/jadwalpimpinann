      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Dashboard
            <small>Control panel</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
          </ol>
        </section>
        <section class="content">
                  <div class="row">
                    <div class="col-xs-12">
                      <div class="box bg-gray disabled color-palette">
                        <div class="box-header">
                          <h3 class="box-title">Matriks Kegiatan Pimpinan</h3>
                        </div> <!-- /.box-header -->

                      <?php

                      $senin = date("d-M-Y", strtotime($senin));
                      $selasa = date("d-M-Y", strtotime($selasa));
                      $rabu = date("d-M-Y", strtotime($rabu));
                      $kamis = date("d-M-Y", strtotime($kamis));
                      $jumat = date("d-M-Y", strtotime($jumat));
                      $sabtu = date("d-M-Y", strtotime($sabtu));
                      $minggu = date("d-M-Y", strtotime($minggu));


                      ?>
                        <div class="box-body table-responsive">
                          <table class="table table-hover border" border="2">
                            <tr class="bg-maroon color-palette">
                              <th style="width:12%">Hari/Tanggal</th>
                              <th style="width:12%">Ka. BPSDM</th>
                              <th style="width:12%">Ses. BPSDM</th>
                              <th style="width:12%">Ka. PusFungHAM</th>
                              <th style="width:12%">Ka. PusPenKOm</th>
                              <th style="width:12%">Ka. PusTekpim</th>
                              <th style="width:12%">Dir. Poltekip</th>
                              <th style="width:12%">Dir. Poltekim</th>
                            </tr>
                            <tr>
                              <td>Senin <br> <?php echo $senin;?></td>
                              <td><?php foreach ($jadwalsenin['kabadan'] as $item) {
                                  ?> <div class="box <?php
                                  if ($item['kabadan']==10) { echo "bg-purple color-palette";}
                                  if ($item['kabadan']==1) { echo "bg-green disabled color-palette";}
                                  if ($item['kabadan']==2) { echo "bg-orange disabled color-palette";}
                                  if ($item['kabadan']==4) { echo "";}
                                  ?> "> <?php echo "KEGIATAN : <br>".$item['kegiatan']." <br> <br> LOKASI :  <br>".$item['tempat']."<br> <br> PUKUL : <br>".$item['waktu']."<br>"; ?> </div> <?php
                                }
                                ?></td>
                                <td><?php foreach ($jadwalsenin['ses'] as $item) {
                                    ?> <div class="box <?php if ($item['ses']==10) { echo "bg-purple color-palette";}
                                    if ($item['ses']==1) { echo "bg-green disabled color-palette";}
                                    if ($item['ses']==2) { echo "bg-orange disabled color-palette";} ?> "> <?php echo "KEGIATAN : <br>".$item['kegiatan']." <br> <br> LOKASI :  <br>".$item['tempat']."<br> <br> PUKUL : <br>".$item['waktu']."<br>"; ?> </div> <?php
                                  }
                                  ?></td>
                                  <td><?php foreach ($jadwalsenin['fungham'] as $item) {
                                      ?> <div class="box <?php if ($item['fungham']==10) { echo "bg-purple color-palette";}
                                      if ($item['fungham']==1) { echo "bg-green disabled color-palette";}
                                      if ($item['fungham']==2) { echo "bg-orange disabled color-palette";} ?> "> <?php echo "KEGIATAN : <br>".$item['kegiatan']." <br> <br> LOKASI :  <br>".$item['tempat']."<br> <br> PUKUL : <br>".$item['waktu']."<br>"; ?> </div> <?php
                                    }
                                    ?></td>
                              <td><?php foreach ($jadwalsenin['penkom'] as $item) {
                                  ?> <div class="box <?php if ($item['penkom']==10) { echo "bg-purple color-palette";}
                                  if ($item['penkom']==1) { echo "bg-green disabled color-palette";}
                                  if ($item['penkom']==2) { echo "bg-orange disabled color-palette";} ?> "> <?php echo "KEGIATAN : <br>".$item['kegiatan']." <br> <br> LOKASI :  <br>".$item['tempat']."<br> <br> PUKUL : <br>".$item['waktu']."<br>"; ?> </div> <?php
                                }
                                ?></td>
                                <td><?php foreach ($jadwalsenin['tekpim'] as $item) {
                                    ?> <div class="box <?php if ($item['tekpim']==10) { echo "bg-purple color-palette";}
                                    if ($item['tekpim']==1) { echo "bg-green disabled color-palette";}
                                    if ($item['tekpim']==2) { echo "bg-orange disabled color-palette";} ?> "> <?php echo "KEGIATAN : <br>".$item['kegiatan']." <br> <br> LOKASI :  <br>".$item['tempat']."<br> <br> PUKUL : <br>".$item['waktu']."<br>"; ?> </div> <?php
                                  }
                                  ?></td>
                                  <td><?php foreach ($jadwalsenin['poltekip'] as $item) {
                                      ?> <div class="box <?php if ($item['poltekip']==10) { echo "bg-purple color-palette";}
                                      if ($item['poltekip']==1) { echo "bg-green disabled color-palette";}
                                      if ($item['poltekip']==2) { echo "bg-orange disabled color-palette";} ?> "> <?php echo "KEGIATAN : <br>".$item['kegiatan']." <br> <br> LOKASI :  <br>".$item['tempat']."<br> <br> PUKUL : <br>".$item['waktu']."<br>"; ?> </div> <?php
                                    }
                                    ?></td>
                                    <td><?php foreach ($jadwalsenin['poltekim'] as $item) {
                                        ?> <div class="box <?php if ($item['poltekim']==10) { echo "bg-purple color-palette";}
                                        if ($item['poltekim']==1) { echo "bg-green disabled color-palette";}
                                        if ($item['poltekim']==2) { echo "bg-orange disabled color-palette";} ?> "> <?php echo "KEGIATAN : <br>".$item['kegiatan']." <br> <br> LOKASI :  <br>".$item['tempat']."<br> <br> PUKUL : <br>".$item['waktu']."<br>"; ?> </div> <?php
                                      }
                                      ?></td>
                            </tr>
                            <tr>
                              <td>Selasa <br> <?php echo $selasa;?></td>
                              <td><?php foreach ($jadwalselasa['kabadan'] as $item) {
                                  ?> <div class="box <?php
                                  if ($item['kabadan']==10) { echo "bg-purple color-palette";}
                                  if ($item['kabadan']==1) { echo "bg-green disabled color-palette";}
                                  if ($item['kabadan']==2) { echo "bg-orange disabled color-palette";}
                                  if ($item['kabadan']==4) { echo "";}
                                   ?> "> <?php echo "KEGIATAN : <br>".$item['kegiatan']." <br> <br> LOKASI :  <br>".$item['tempat']."<br> <br> PUKUL : <br>".$item['waktu']."<br>"; ?> </div> <?php
                                }
                                ?></td>
                                <td><?php foreach ($jadwalselasa['ses'] as $item) {
                                    ?> <div class="box <?php if ($item['ses']==10) { echo "bg-purple color-palette";}
                                    if ($item['ses']==1) { echo "bg-green disabled color-palette";}
                                    if ($item['ses']==2) { echo "bg-orange disabled color-palette";} ?> "> <?php echo "KEGIATAN : <br>".$item['kegiatan']." <br> <br> LOKASI :  <br>".$item['tempat']."<br> <br> PUKUL : <br>".$item['waktu']."<br>"; ?> </div> <?php
                                  }
                                  ?></td>
                                  <td><?php foreach ($jadwalselasa['fungham'] as $item) {
                                      ?> <div class="box <?php if ($item['fungham']==10) { echo "bg-purple color-palette";}
                                      if ($item['fungham']==1) { echo "bg-green disabled color-palette";}
                                      if ($item['fungham']==2) { echo "bg-orange disabled color-palette";} ?> "> <?php echo "KEGIATAN : <br>".$item['kegiatan']." <br> <br> LOKASI :  <br>".$item['tempat']."<br> <br> PUKUL : <br>".$item['waktu']."<br>"; ?> </div> <?php
                                    }
                                    ?></td>
                              <td><?php foreach ($jadwalselasa['penkom'] as $item) {
                                  ?> <div class="box <?php if ($item['penkom']==10) { echo "bg-purple color-palette";}
                                  if ($item['penkom']==1) { echo "bg-green disabled color-palette";}
                                  if ($item['penkom']==2) { echo "bg-orange disabled color-palette";} ?> "> <?php echo "KEGIATAN : <br>".$item['kegiatan']." <br> <br> LOKASI :  <br>".$item['tempat']."<br> <br> PUKUL : <br>".$item['waktu']."<br>"; ?> </div> <?php
                                }
                                ?></td>
                                <td><?php foreach ($jadwalselasa['tekpim'] as $item) {
                                    ?> <div class="box <?php if ($item['tekpim']==10) { echo "bg-purple color-palette";}
                                    if ($item['tekpim']==1) { echo "bg-green disabled color-palette";}
                                    if ($item['tekpim']==2) { echo "bg-orange disabled color-palette";} ?> "> <?php echo "KEGIATAN : <br>".$item['kegiatan']." <br> <br> LOKASI :  <br>".$item['tempat']."<br> <br> PUKUL : <br>".$item['waktu']."<br>"; ?> </div> <?php
                                  }
                                  ?></td>
                                  <td><?php foreach ($jadwalselasa['poltekip'] as $item) {
                                      ?> <div class="box <?php if ($item['poltekip']==10) { echo "bg-purple color-palette";}
                                      if ($item['poltekip']==1) { echo "bg-green disabled color-palette";}
                                      if ($item['poltekip']==2) { echo "bg-orange disabled color-palette";} ?> "> <?php echo "KEGIATAN : <br>".$item['kegiatan']." <br> <br> LOKASI :  <br>".$item['tempat']."<br> <br> PUKUL : <br>".$item['waktu']."<br>"; ?> </div> <?php
                                    }
                                    ?></td>
                                    <td><?php foreach ($jadwalselasa['poltekim'] as $item) {
                                        ?> <div class="box <?php if ($item['poltekim']==10) { echo "bg-purple color-palette";}
                                        if ($item['poltekim']==1) { echo "bg-green disabled color-palette";}
                                        if ($item['poltekim']==2) { echo "bg-orange disabled color-palette";} ?> "> <?php echo "KEGIATAN : <br>".$item['kegiatan']." <br> <br> LOKASI :  <br>".$item['tempat']."<br> <br> PUKUL : <br>".$item['waktu']."<br>"; ?> </div> <?php
                                      }
                                      ?></td>
                            </tr>

                            <tr>
                              <td>Rabu <br> <?php echo $rabu;?></td>
                              <td><?php foreach ($jadwalrabu['kabadan'] as $item) {
                                  ?> <div class="box <?php
                                  if ($item['kabadan']==10) { echo "bg-purple color-palette";}
                                  if ($item['kabadan']==1) { echo "bg-green disabled color-palette";}
                                  if ($item['kabadan']==2) { echo "bg-orange disabled color-palette";}
                                  if ($item['kabadan']==4) { echo "";}
                                   ?> "> <?php echo "KEGIATAN : <br>".$item['kegiatan']." <br> <br> LOKASI :  <br>".$item['tempat']."<br> <br> PUKUL : <br>".$item['waktu']."<br>"; ?> </div> <?php
                                }
                                ?></td>
                                <td><?php foreach ($jadwalrabu['ses'] as $item) {
                                    ?> <div class="box <?php if ($item['ses']==10) { echo "bg-purple color-palette";}
                                    if ($item['ses']==1) { echo "bg-green disabled color-palette";}
                                    if ($item['ses']==2) { echo "bg-orange disabled color-palette";} ?> "> <?php echo "KEGIATAN : <br>".$item['kegiatan']." <br> <br> LOKASI :  <br>".$item['tempat']."<br> <br> PUKUL : <br>".$item['waktu']."<br>"; ?> </div> <?php
                                  }
                                  ?></td>
                                  <td><?php foreach ($jadwalrabu['fungham'] as $item) {
                                      ?> <div class="box <?php if ($item['fungham']==10) { echo "bg-purple color-palette";}
                                      if ($item['fungham']==1) { echo "bg-green disabled color-palette";}
                                      if ($item['fungham']==2) { echo "bg-orange disabled color-palette";} ?> "> <?php echo "KEGIATAN : <br>".$item['kegiatan']." <br> <br> LOKASI :  <br>".$item['tempat']."<br> <br> PUKUL : <br>".$item['waktu']."<br>"; ?> </div> <?php
                                    }
                                    ?></td>
                              <td><?php foreach ($jadwalrabu['penkom'] as $item) {
                                  ?> <div class="box <?php if ($item['penkom']==10) { echo "bg-purple color-palette";}
                                  if ($item['penkom']==1) { echo "bg-green disabled color-palette";}
                                  if ($item['penkom']==2) { echo "bg-orange disabled color-palette";} ?> "> <?php echo "KEGIATAN : <br>".$item['kegiatan']." <br> <br> LOKASI :  <br>".$item['tempat']."<br> <br> PUKUL : <br>".$item['waktu']."<br>"; ?> </div> <?php
                                }
                                ?></td>
                                <td><?php foreach ($jadwalrabu['tekpim'] as $item) {
                                    ?> <div class="box <?php if ($item['tekpim']==10) { echo "bg-purple color-palette";}
                                    if ($item['tekpim']==1) { echo "bg-green disabled color-palette";}
                                    if ($item['tekpim']==2) { echo "bg-orange disabled color-palette";} ?> "> <?php echo "KEGIATAN : <br>".$item['kegiatan']." <br> <br> LOKASI :  <br>".$item['tempat']."<br> <br> PUKUL : <br>".$item['waktu']."<br>"; ?> </div> <?php
                                  }
                                  ?></td>
                                  <td><?php foreach ($jadwalrabu['poltekip'] as $item) {
                                      ?> <div class="box <?php if ($item['poltekip']==10) { echo "bg-purple color-palette";}
                                      if ($item['poltekip']==1) { echo "bg-green disabled color-palette";}
                                      if ($item['poltekip']==2) { echo "bg-orange disabled color-palette";} ?> "> <?php echo "KEGIATAN : <br>".$item['kegiatan']." <br> <br> LOKASI :  <br>".$item['tempat']."<br> <br> PUKUL : <br>".$item['waktu']."<br>"; ?> </div> <?php
                                    }
                                    ?></td>
                                    <td><?php foreach ($jadwalrabu['poltekim'] as $item) {
                                        ?> <div class="box <?php if ($item['poltekim']==10) { echo "bg-purple color-palette";}
                                        if ($item['poltekim']==1) { echo "bg-green disabled color-palette";}
                                        if ($item['poltekim']==2) { echo "bg-orange disabled color-palette";} ?> "> <?php echo "KEGIATAN : <br>".$item['kegiatan']." <br> <br> LOKASI :  <br>".$item['tempat']."<br> <br> PUKUL : <br>".$item['waktu']."<br>"; ?> </div> <?php
                                      }
                                      ?></td>
                            </tr>

                            <tr>
                              <td>Kamis <br> <?php echo $kamis;?></td>
                              <td><?php foreach ($jadwalkamis['kabadan'] as $item) {
                                  ?> <div class="box <?php
                                  if ($item['kabadan']==10) { echo "bg-purple color-palette";}
                                  if ($item['kabadan']==1) { echo "bg-green disabled color-palette";}
                                  if ($item['kabadan']==2) { echo "bg-orange disabled color-palette";}
                                  if ($item['kabadan']==4) { echo "";}
                                   ?> "> <?php echo "KEGIATAN : <br>".$item['kegiatan']." <br> <br> LOKASI :  <br>".$item['tempat']."<br> <br> PUKUL : <br>".$item['waktu']."<br>"; ?> </div> <?php
                                }
                                ?></td>
                                <td><?php foreach ($jadwalkamis['ses'] as $item) {
                                    ?> <div class="box <?php if ($item['ses']==10) { echo "bg-purple color-palette";}
                                    if ($item['ses']==1) { echo "bg-green disabled color-palette";}
                                    if ($item['ses']==2) { echo "bg-orange disabled color-palette";} ?> "> <?php echo "KEGIATAN : <br>".$item['kegiatan']." <br> <br> LOKASI :  <br>".$item['tempat']."<br> <br> PUKUL : <br>".$item['waktu']."<br>"; ?> </div> <?php
                                  }
                                  ?></td>
                                  <td><?php foreach ($jadwalkamis['fungham'] as $item) {
                                      ?> <div class="box <?php if ($item['fungham']==10) { echo "bg-purple color-palette";}
                                      if ($item['fungham']==1) { echo "bg-green disabled color-palette";}
                                      if ($item['fungham']==2) { echo "bg-orange disabled color-palette";} ?> "> <?php echo "KEGIATAN : <br>".$item['kegiatan']." <br> <br> LOKASI :  <br>".$item['tempat']."<br> <br> PUKUL : <br>".$item['waktu']."<br>"; ?> </div> <?php
                                    }
                                    ?></td>
                              <td><?php foreach ($jadwalkamis['penkom'] as $item) {
                                  ?> <div class="box <?php if ($item['penkom']==10) { echo "bg-purple color-palette";}
                                  if ($item['penkom']==1) { echo "bg-green disabled color-palette";}
                                  if ($item['penkom']==2) { echo "bg-orange disabled color-palette";} ?> "> <?php echo "KEGIATAN : <br>".$item['kegiatan']." <br> <br> LOKASI :  <br>".$item['tempat']."<br> <br> PUKUL : <br>".$item['waktu']."<br>"; ?> </div> <?php
                                }
                                ?></td>
                                <td><?php foreach ($jadwalkamis['tekpim'] as $item) {
                                    ?> <div class="box <?php if ($item['tekpim']==10) { echo "bg-purple color-palette";}
                                    if ($item['tekpim']==1) { echo "bg-green disabled color-palette";}
                                    if ($item['tekpim']==2) { echo "bg-orange disabled color-palette";} ?> "> <?php echo "KEGIATAN : <br>".$item['kegiatan']." <br> <br> LOKASI :  <br>".$item['tempat']."<br> <br> PUKUL : <br>".$item['waktu']."<br>"; ?> </div> <?php
                                  }
                                  ?></td>
                                  <td><?php foreach ($jadwalkamis['poltekip'] as $item) {
                                      ?> <div class="box <?php if ($item['poltekip']==10) { echo "bg-purple color-palette";}
                                      if ($item['poltekip']==1) { echo "bg-green disabled color-palette";}
                                      if ($item['poltekip']==2) { echo "bg-orange disabled color-palette";} ?> "> <?php echo "KEGIATAN : <br>".$item['kegiatan']." <br> <br> LOKASI :  <br>".$item['tempat']."<br> <br> PUKUL : <br>".$item['waktu']."<br>"; ?> </div> <?php
                                    }
                                    ?></td>
                                    <td><?php foreach ($jadwalkamis['poltekim'] as $item) {
                                        ?> <div class="box <?php if ($item['poltekim']==10) { echo "bg-purple color-palette";}
                                        if ($item['poltekim']==1) { echo "bg-green disabled color-palette";}
                                        if ($item['poltekim']==2) { echo "bg-orange disabled color-palette";} ?> "> <?php echo "KEGIATAN : <br>".$item['kegiatan']." <br> <br> LOKASI :  <br>".$item['tempat']."<br> <br> PUKUL : <br>".$item['waktu']."<br>"; ?> </div> <?php
                                      }
                                      ?></td>
                            </tr>

                            <tr>
                              <td>Jumat <br> <?php echo $jumat;?></td>
                              <td><?php foreach ($jadwaljumat['kabadan'] as $item) {
                                  ?> <div class="box <?php if ($item['kabadan']==10) { echo "bg-purple color-palette";}
                                  if ($item['kabadan']==1) { echo "bg-green disabled color-palette";}
                                  if ($item['kabadan']==2) { echo "bg-orange disabled color-palette";}
                                  if ($item['kabadan']==4) { echo "";} ?> "> <?php echo "KEGIATAN : <br>".$item['kegiatan']." <br> <br> LOKASI :  <br>".$item['tempat']."<br> <br> PUKUL : <br>".$item['waktu']."<br>"; ?> </div> <?php
                                }
                                ?></td>
                                <td><?php foreach ($jadwaljumat['ses'] as $item) {
                                    ?> <div class="box <?php if ($item['ses']==10) { echo "bg-purple color-palette";}
                                    if ($item['ses']==1) { echo "bg-green disabled color-palette";}
                                    if ($item['ses']==2) { echo "bg-orange disabled color-palette";} ?> "> <?php echo "KEGIATAN : <br>".$item['kegiatan']." <br> <br> LOKASI :  <br>".$item['tempat']."<br> <br> PUKUL : <br>".$item['waktu']."<br>"; ?> </div> <?php
                                  }
                                  ?></td>
                                  <td><?php foreach ($jadwaljumat['fungham'] as $item) {
                                      ?> <div class="box <?php if ($item['fungham']==10) { echo "bg-purple color-palette";}
                                      if ($item['fungham']==1) { echo "bg-green disabled color-palette";}
                                      if ($item['fungham']==2) { echo "bg-orange disabled color-palette";} ?> "> <?php echo "KEGIATAN : <br>".$item['kegiatan']." <br> <br> LOKASI :  <br>".$item['tempat']."<br> <br> PUKUL : <br>".$item['waktu']."<br>"; ?> </div> <?php
                                    }
                                    ?></td>
                              <td><?php foreach ($jadwaljumat['penkom'] as $item) {
                                  ?> <div class="box <?php if ($item['penkom']==10) { echo "bg-purple color-palette";}
                                  if ($item['penkom']==1) { echo "bg-green disabled color-palette";}
                                  if ($item['penkom']==2) { echo "bg-orange disabled color-palette";} ?> "> <?php echo "KEGIATAN : <br>".$item['kegiatan']." <br> <br> LOKASI :  <br>".$item['tempat']."<br> <br> PUKUL : <br>".$item['waktu']."<br>"; ?> </div> <?php
                                }
                                ?></td>
                                <td><?php foreach ($jadwaljumat['tekpim'] as $item) {
                                    ?> <div class="box <?php if ($item['tekpim']==10) { echo "bg-purple color-palette";}
                                    if ($item['tekpim']==1) { echo "bg-green disabled color-palette";}
                                    if ($item['tekpim']==2) { echo "bg-orange disabled color-palette";} ?> "> <?php echo "KEGIATAN : <br>".$item['kegiatan']." <br> <br> LOKASI :  <br>".$item['tempat']."<br> <br> PUKUL : <br>".$item['waktu']."<br>"; ?> </div> <?php
                                  }
                                  ?></td>
                                  <td><?php foreach ($jadwaljumat['poltekip'] as $item) {
                                      ?> <div class="box <?php if ($item['poltekip']==10) { echo "bg-purple color-palette";}
                                      if ($item['poltekip']==1) { echo "bg-green disabled color-palette";}
                                      if ($item['poltekip']==2) { echo "bg-orange disabled color-palette";} ?> "> <?php echo "KEGIATAN : <br>".$item['kegiatan']." <br> <br> LOKASI :  <br>".$item['tempat']."<br> <br> PUKUL : <br>".$item['waktu']."<br>"; ?> </div> <?php
                                    }
                                    ?></td>
                                    <td><?php foreach ($jadwaljumat['poltekim'] as $item) {
                                        ?> <div class="box <?php if ($item['poltekim']==10) { echo "bg-purple color-palette";}
                                        if ($item['poltekim']==1) { echo "bg-green disabled color-palette";}
                                        if ($item['poltekim']==2) { echo "bg-orange disabled color-palette";} ?> "> <?php echo "KEGIATAN : <br>".$item['kegiatan']." <br> <br> LOKASI :  <br>".$item['tempat']."<br> <br> PUKUL : <br>".$item['waktu']."<br>"; ?> </div> <?php
                                      }
                                      ?></td>
                            </tr>
                            <tr>
                              <td>Sabtu <br> <?php echo $sabtu;?></td>
                              <td><?php foreach ($jadwalsabtu['kabadan'] as $item) {
                                  ?> <div class="box <?php if ($item['kabadan']==10) { echo "bg-purple color-palette";}
                                  if ($item['kabadan']==1) { echo "bg-green disabled color-palette";}
                                  if ($item['kabadan']==2) { echo "bg-orange disabled color-palette";}
                                  if ($item['kabadan']==4) { echo "";} ?> "> <?php echo "KEGIATAN : <br>".$item['kegiatan']." <br> <br> LOKASI :  <br>".$item['tempat']."<br> <br> PUKUL : <br>".$item['waktu']."<br>"; ?> </div> <?php
                                }
                                ?></td>
                                <td><?php foreach ($jadwalsabtu['ses'] as $item) {
                                    ?> <div class="box <?php if ($item['ses']==10) { echo "bg-purple color-palette";}
                                    if ($item['ses']==1) { echo "bg-green disabled color-palette";}
                                    if ($item['ses']==2) { echo "bg-orange disabled color-palette";} ?> "> <?php echo "KEGIATAN : <br>".$item['kegiatan']." <br> <br> LOKASI :  <br>".$item['tempat']."<br> <br> PUKUL : <br>".$item['waktu']."<br>"; ?> </div> <?php
                                  }
                                  ?></td>
                                  <td><?php foreach ($jadwalsabtu['fungham'] as $item) {
                                      ?> <div class="box <?php if ($item['fungham']==10) { echo "bg-purple color-palette";}
                                      if ($item['fungham']==1) { echo "bg-green disabled color-palette";}
                                      if ($item['fungham']==2) { echo "bg-orange disabled color-palette";} ?> "> <?php echo "KEGIATAN : <br>".$item['kegiatan']." <br> <br> LOKASI :  <br>".$item['tempat']."<br> <br> PUKUL : <br>".$item['waktu']."<br>"; ?> </div> <?php
                                    }
                                    ?></td>
                              <td><?php foreach ($jadwalsabtu['penkom'] as $item) {
                                  ?> <div class="box <?php if ($item['penkom']==10) { echo "bg-purple color-palette";}
                                  if ($item['penkom']==1) { echo "bg-green disabled color-palette";}
                                  if ($item['penkom']==2) { echo "bg-orange disabled color-palette";} ?> "> <?php echo "KEGIATAN : <br>".$item['kegiatan']." <br> <br> LOKASI :  <br>".$item['tempat']."<br> <br> PUKUL : <br>".$item['waktu']."<br>"; ?> </div> <?php
                                }
                                ?></td>
                                <td><?php foreach ($jadwalsabtu['tekpim'] as $item) {
                                    ?> <div class="box <?php if ($item['tekpim']==10) { echo "bg-purple color-palette";}
                                    if ($item['tekpim']==1) { echo "bg-green disabled color-palette";}
                                    if ($item['tekpim']==2) { echo "bg-orange disabled color-palette";} ?> "> <?php echo "KEGIATAN : <br>".$item['kegiatan']." <br> <br> LOKASI :  <br>".$item['tempat']."<br> <br> PUKUL : <br>".$item['waktu']."<br>"; ?> </div> <?php
                                  }
                                  ?></td>
                                  <td><?php foreach ($jadwalsabtu['poltekip'] as $item) {
                                      ?> <div class="box <?php if ($item['poltekip']==10) { echo "bg-purple color-palette";}
                                      if ($item['poltekip']==1) { echo "bg-green disabled color-palette";}
                                      if ($item['poltekip']==2) { echo "bg-orange disabled color-palette";} ?> "> <?php echo "KEGIATAN : <br>".$item['kegiatan']." <br> <br> LOKASI :  <br>".$item['tempat']."<br> <br> PUKUL : <br>".$item['waktu']."<br>"; ?> </div> <?php
                                    }
                                    ?></td>
                                    <td><?php foreach ($jadwalsabtu['poltekim'] as $item) {
                                        ?> <div class="box <?php if ($item['poltekim']==10) { echo "bg-purple color-palette";}
                                        if ($item['poltekim']==1) { echo "bg-green disabled color-palette";}
                                        if ($item['poltekim']==2) { echo "bg-orange disabled color-palette";} ?> "> <?php echo "KEGIATAN : <br>".$item['kegiatan']." <br> <br> LOKASI :  <br>".$item['tempat']."<br> <br> PUKUL : <br>".$item['waktu']."<br>"; ?> </div> <?php
                                      }
                                      ?></td>
                            </tr>

                            <tr>
                              <td>Minggu <br> <?php echo $minggu;?></td>
                              <td><?php foreach ($jadwalminggu['kabadan'] as $item) {
                                  ?> <div class="box <?php if ($item['kabadan']==10) { echo "bg-purple color-palette";}
                                  if ($item['kabadan']==1) { echo "bg-green disabled color-palette";}
                                  if ($item['kabadan']==2) { echo "bg-orange disabled color-palette";}
                                  if ($item['kabadan']==4) { echo "";}?> "> <?php echo "KEGIATAN : <br>".$item['kegiatan']." <br> <br> LOKASI :  <br>".$item['tempat']."<br> <br> PUKUL : <br>".$item['waktu']."<br>"; ?> </div> <?php
                                }
                                ?></td>
                                <td><?php foreach ($jadwalminggu['ses'] as $item) {
                                    ?> <div class="box <?php if ($item['ses']==10) { echo "bg-purple color-palette";}
                                    if ($item['ses']==1) { echo "bg-green disabled color-palette";}
                                    if ($item['ses']==2) { echo "bg-orange disabled color-palette";} ?> "> <?php echo "KEGIATAN : <br>".$item['kegiatan']." <br> <br> LOKASI :  <br>".$item['tempat']."<br> <br> PUKUL : <br>".$item['waktu']."<br>"; ?> </div> <?php
                                  }
                                  ?></td>
                                  <td><?php foreach ($jadwalminggu['fungham'] as $item) {
                                      ?> <div class="box <?php if ($item['fungham']==10) { echo "bg-purple color-palette";}
                                      if ($item['fungham']==1) { echo "bg-green disabled color-palette";}
                                      if ($item['fungham']==2) { echo "bg-orange disabled color-palette";} ?> "> <?php echo "KEGIATAN : <br>".$item['kegiatan']." <br> <br> LOKASI :  <br>".$item['tempat']."<br> <br> PUKUL : <br>".$item['waktu']."<br>"; ?> </div> <?php
                                    }
                                    ?></td>
                              <td><?php foreach ($jadwalminggu['penkom'] as $item) {
                                  ?> <div class="box <?php if ($item['penkom']==10) { echo "bg-purple color-palette";}
                                  if ($item['penkom']==1) { echo "bg-green disabled color-palette";}
                                  if ($item['penkom']==2) { echo "bg-orange disabled color-palette";} ?>"> <?php echo "KEGIATAN : <br>".$item['kegiatan']." <br> <br> LOKASI :  <br>".$item['tempat']."<br> <br> PUKUL : <br>".$item['waktu']."<br>"; ?> </div> <?php
                                }
                                ?></td>
                                <td><?php foreach ($jadwalminggu['tekpim'] as $item) {
                                    ?> <div class="box <?php if ($item['tekpim']==10) { echo "bg-purple color-palette";}
                                    if ($item['tekpim']==1) { echo "bg-green disabled color-palette";}
                                    if ($item['tekpim']==2) { echo "bg-orange disabled color-palette";} ?> "> <?php echo "KEGIATAN : <br>".$item['kegiatan']." <br> <br> LOKASI :  <br>".$item['tempat']."<br> <br> PUKUL : <br>".$item['waktu']."<br>"; ?> </div> <?php
                                  }
                                  ?></td>
                                  <td><?php foreach ($jadwalminggu['poltekip'] as $item) {
                                      ?> <div class="box <?php if ($item['poltekip']==10) { echo "bg-purple color-palette";}
                                      if ($item['poltekip']==1) { echo "bg-green disabled color-palette";}
                                      if ($item['poltekip']==2) { echo "bg-orange disabled color-palette";} ?> "> <?php echo "KEGIATAN : <br>".$item['kegiatan']." <br> <br> LOKASI :  <br>".$item['tempat']."<br> <br> PUKUL : <br>".$item['waktu']."<br>"; ?> </div> <?php
                                    }
                                    ?></td>
                                    <td><?php foreach ($jadwalminggu['poltekim'] as $item) {
                                        ?> <div class="box <?php if ($item['poltekim']==10) { echo "bg-purple color-palette";}
                                        if ($item['poltekim']==1) { echo "bg-green disabled color-palette";}
                                        if ($item['poltekim']==2) { echo "bg-orange disabled color-palette";} ?> "> <?php echo "KEGIATAN : <br>".$item['kegiatan']." <br> <br> LOKASI :  <br>".$item['tempat']."<br> <br> PUKUL : <br>".$item['waktu']."<br>"; ?> </div> <?php
                                      }
                                      ?></td>
                            </tr>


                          </table>
                        </div><!-- /.box-body -->

                        <div class="row col-xs-5">
                          <div class="bg-green disabled color-palette col-xs-2">Hadir</div> <div class="col-xs-1"></div>
                          <div class="bg-orange disabled color-palette col-xs-2">Diwakili</div> <div class="col-xs-1"></div>
                          <div class="bg-purple color-palette col-xs-2">Tidak Hadir</div> <div class="col-xs-1"></div>
                          <div class="col-xs-2">Tentative</div>
                        </div>
                      </div><!-- /.box -->
                    </div>
                  </div>
          </section>



        <!-- Main content -->
      </div><!-- /.content-wrapper -->
