      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Dashboard
            <small>Control panel</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
          </ol>
        </section>
        <?php $user=$this->session->userdata('user');
              if ($user== "tukabadan") { $pimpinan="Kepala BPSDM"; }
              if ($user=="tuses") { $pimpinan="Sekretaris BPSDM"; }
              if ($user=="tufungham") { $pimpinan="Kepala PusFungHAM"; }
              if ($user=="tupenkom") { $pimpinan="Kepala PusPenKOm"; }
              if ($user=="tutekpim") { $pimpinan="Kepala PusTekPim"; }
              if ($user=="tupoltekip") { $pimpinan="Direktur Poltekip"; }
              if ($user=="tupoltekim") { $pimpinan="Direktur Poltekim"; } ?>
        <section class="content">
                  <div class="row ">
                    <div class="col-xs-12 ">
                      <div class="box bg-gray disabled color-palette">
                        <div class="box-header">
                          <h3 class="box-title">Jadwal Kegiatan <?php echo $pimpinan; ?></h3>
                        </div><!-- /.box-header -->
                        <div class="btn-gorup">
                          <a href="<?php echo base_url(); ?>jadwal/pimpinan" class="btn btn-info"> All Agenda </a>
                          <!--<a href="<?php echo base_url(); ?>jadwal/dihadiri" class="btn btn-info"> Agenda Yang Dihadiri</a> -->
                          <a href="<?php echo base_url(); ?>jadwal/cetak" class="btn btn-info"> Cetak </a>
                          <a href="<?php echo base_url(); ?>jadwal/tambah" class="btn btn-info pull-right"> + Tambah </a>
                        </div>
                        <div class="row col-xs-5">
                          <div class="bg-green disabled color-palette col-xs-2">Hadir</div> <div class="col-xs-1"></div>
                          <div class="bg-orange disabled color-palette col-xs-2">Diwakili</div> <div class="col-xs-1"></div>
                          <div class="bg-purple color-palette col-xs-3">Tidak Hadir</div> <div class="col-xs-1"></div>
                        </div>
                            <table class="table table-hover" border='2'>
                            <tr class="bg-maroon color-palette">
                              <th style="width:10%">Hari/Tanggal</th>
                              <th style="width:10%">Waktu</th>
                              <th style="width:10%">Kegiatan</th>
                              <th style="width:10%">Tempat</th>
                              <th style="width:10%">Keterangan</th>
                              <th style="width:10%">Disposisi/Tindak Lanjut</th>
                              <th style="width:10%">Option</th>
                            </tr>
                            <?php foreach ($jadwal as $item) {
                            $user=$this->session->userdata('user');
                            if ($user== "tukabadan") { $hadir=$item['kabadan']; }
                      			if ($user=="tuses") { $hadir=$item['ses']; }
                      			if ($user=="tufungham") { $hadir=$item['fungham']; }
                      			if ($user=="tupenkom") { $hadir=$item['penkom']; }
                      			if ($user=="tutekpim") { $hadir=$item['tekpim']; }
                      			if ($user=="tupoltekip") { $hadir=$item['poltekip']; }
                      			if ($user=="tupoltekim") {$hadir=$item['poltekim']; } ?>
                            <tr class="<?php  if ($hadir==10) { echo "bg-purple color-palette";}
                            if ($hadir==1) { echo "bg-green disabled color-palette";}
                            if ($hadir==2) { echo "bg-orange disabled color-palette";}
                            //if ($hadir==4) { echo "";} ?>">
                              <td> <?php $item['tanggal'] = date("d-M-Y", strtotime($item['tanggal']));
                              echo $item['tanggal']; ?> </td>
                              <td><?php echo $item['waktu']; ?></td>
                              <td><?php echo $item['kegiatan']; ?></td>
                              <td><?php echo $item['tempat']; ?></span></td>
                              <td>
                                Materi :  <?php echo $item['materi']; ?><br> <br>
                                Pejabat Turut Diundang : <br><?php
                                if ($item['kabadan']) echo "Kepala BPSDM <br>";
                                if ($item['ses']) echo "Ses. BPSDM <br>";
                                if ($item['ses'] || $item ['kabadan']) echo $item['lainses']."<br><br>";
                                if ($item['fungham']) echo "Ka. Pusfungham <br>".$item['lainfungham']."<br><br>";
                                if ($item['penkom']) echo "Ka. Puspenkom <br>".$item['lainpenkom']."<br><br>";
                                if ($item['tekpim']) echo "Ka. Pustekpim <br>".$item['laintekpim']."<br><br>";
                                if ($item['poltekip']) echo "Dir. Poltekip <br>".$item['lainpoltekip']."<br><br>";
                                if ($item['poltekim']) echo "Dir. Poltekim <br>".$item['lainpoltekip']."<br><br>";
                                 ?> <br> <br>
                                Kategori : <?php echo $item['kategori']; ?> <br> <br>
                                Contact Person : <?php echo $item['cp']; ?> <br> <br>
                                Dress Code : <?php echo $item['dresscode']; ?> <br> <br>
                                Lain-lain : <?php echo $item['keterangan']; ?> <br> <br>
                                File : <?php if ($item['file']) { ?> <a href="<?php echo base_url()."uploads/".$item['file'];  ?>"  class="btn btn-block btn-info btn sm"> Download File </a> <?php } ?>
                              </td>
                              <td>
                                <?php if ($this->session->userdata('user')=='tukabadan') {?>
                                <a href="<?php echo base_url(); ?>jadwal/disposisi/<?php echo $item['id'];  ?>"  class="btn btn-block btn-info btn sm"> Disposisi </a><br> <?php } ?>
                                Disposisi : <br> <?php echo $item['disposisi']; ?>
                                <?php if (!$this->session->userdata('user')=='tukabadan') {?>
                                <a href="<?php echo base_url(); ?>jadwal/wakili/didampingi/<?php echo $item['id'];  ?>"  class="btn btn-block btn-info btn sm"> Disposisi </a><br> <?php } ?>

                              </td>
                              <td>

                                  <form action="<?php echo base_url(); ?>/jadwal/hadir/" role="form" method="post">
                                      <div class="box-body">
                                        <div class="form-group col-xs-12">
                                            <div class="radio">
                                              <label>
                                                <input type="radio" name="hadir" id="optionsRadios1" value="1" <?php if ($hadir==1) { echo "checked"; }?> >
                                                Hadir
                                              </label>
                                            </div>
                                            <div class="radio">
                                              <label>
                                                <input type="radio" name="hadir" id="optionsRadios2" value="10" <?php if ($hadir==10) { echo "checked"; } ?>>
                                                Tidak Hadir
                                                </label>
                                            </div>
                                            <div class="radio">
                                              <label>
                                                <input type="radio" name="hadir" id="optionsRadios2" value="2" <?php if ($hadir==2) { echo "checked"; } ?>>
                                                Diwakili
                                                </label>
                                            </div>
                                            <div class="radio">
                                              <label>
                                                <input type="radio" name="hadir" id="optionsRadios3" value=NULL >
                                                Tentative
                                              </label>
                                            </div>
                                            <div class="form-group">
                                              <label for="exampleInputEmail1">Diwakili/Didampingi</label>
                                              <?php
                                              if ($user== "tukabadan") { $item['lain']=$item['lainses']; }
                                              if ($user=="tuses") { $item['lain']=$item['lainses']; }
                                              if ($user=="tufungham") { $item['lain']=$item['lainfungham']; }
                                              if ($user=="tupenkom") { $item['lain']=$item['lainpenkom']; }
                                              if ($user=="tutekpim") { $item['lain']=$item['laintekpim']; }
                                              if ($user=="tupoltekip") { $item['lain']=$item['lainpoltekip']; }
                                              if ($user=="tupoltekim") {$item['lain']=$item['lainpoltekim']; } ?>
                                              <input class ="col-xs-12" type="text" class="form-control" name='lain' STYLE="color: #000000"; value="<?php echo $item['lain']; ?>">
                                            </div>
                                            <input type="hidden" name="id" value="<?php echo $item['id'] ?>"/>
                                          </div>
                                        </div>
                                      <button type="submit" class="btn btn-primary">Submit</button>
                                      <br>

                                  </form>



                                <?php if ($this->session->userdata('user')==$item['dibuat']) {?>
                                <a href="<?php echo base_url(); ?>jadwal/update/<?php echo $item['id'];  ?>"  class="btn btn-block btn-info btn sm"> Update </a><br> <?php } ?>
                                <?php if ($this->session->userdata('user')==$item['dibuat']) {?>
                                <a href="<?php echo base_url(); ?>jadwal/hapus/<?php echo $item['id'];  ?>"  class="btn btn-block btn-danger btn sm"> Hapus </a> <br> <?php } ?>

                                Dibuat oleh : <?php echo $item['dibuat'];?><br>
                                Last Update : <?php echo $item['lastupdate'];?>
                              </td>
                            </tr>
                          <?php } ?>
                          </table>

                      </div><!-- /.box -->
                    </div>
                  </div>
                </section>



        <!-- Main content -->
      </div><!-- /.content-wrapper -->
