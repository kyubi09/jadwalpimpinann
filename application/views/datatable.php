<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>E-Schedule BPSDM</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo base_url().'/assets/'; ?>bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url().'/assets/'; ?>bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo base_url().'/assets/'; ?>bower_components/Ionicons/css/ionicons.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="<?php echo base_url().'/assets/'; ?>bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url().'/assets/'; ?>dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo base_url().'/assets/'; ?>dist/css/skins/_all-skins.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="skin-blue sidebar-mini">
  <div class="wrapper">

    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar ">
      <!-- sidebar: style can be found in sidebar.less -->
      <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
          <div class="pull-left image">
            <img src="<?php echo base_url();?>assets/dist/img/bpsdm.png" class="img-circle" alt="User Image" />
          </div>
          <div class="pull-left info">
            <p><?php echo mb_strtoupper($this->session->userdata('user')); ?>
            </p>
            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
          </div>
        </div>

        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
          <li class="header">Jadwal</li>
          <li ><a href="<?php echo base_url();?>jadwal"><i class="fa fa-circle-o"></i> Jadwal Terpadu</a></li>
          <?php $user=$this->session->userdata('user');
                if ($user== "tukabadan") { $pimpinan="Kepala BPSDM"; }
                if ($user=="tuses") { $pimpinan="Sekretaris BPSDM"; }
                if ($user=="tufungham") { $pimpinan="Kepala PusFungHAM"; }
                if ($user=="tupenkom") { $pimpinan="Kepala PusPenKOm"; }
                if ($user=="tutekpim") { $pimpinan="Kepala PusTekPim"; }
                if ($user=="tupoltekip") { $pimpinan="Direktur Poltekip"; }
                if ($user=="tupoltekim") { $pimpinan="Direktur Poltekim"; } ?>

            <?php if ($this->session->userdata('userkode')<4) { ?>
            <li><a href="<?php echo base_url();?>jadwal/pimpinan"><i class="fa fa-clock-o"></i> <?php echo $pimpinan; ?></a></li>
            <li><a href="<?php echo base_url();?>jadwal/search"><i class="fa fa-clock-o"></i> Cari Jadwal</a></li> <?php }
				
			else {
					if ($this->session->userdata('userkode')==4) {?> <li><a href="<?php echo base_url();?>jadwal/gantipass"><i class="fa fa-clock-o"></i> Ganti Password TU</a></li> <?php }
					?>
            <li><a href="<?php echo base_url();?>jadwal/password"><i class="fa fa-clock-o"></i> ubah Password</a></li>
            <li><a href="<?php echo base_url();?>login/logout"><i class="fa fa-clock-o"></i> Logout</a></li>
          </ul>
      </section>
      <!-- /.sidebar -->
    </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Cari Jadwal
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">

          <!-- /.box -->

          <div class="box"> <!-- /.box-header -->

              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr class="bg-maroon color-palette">
                  <th>Kegiatan</th>
                  <th>Tempat</th>
                  <th>Kategori</th>
                  <th>Materi</th>
                  <th>Details</th>
                  </tr>
                </thead>
                <tbody>
                  <?php if($jadwal>=1) { ?>
                  <?php foreach ($jadwal as $item) { ?>
                <tr>
                    <td> <?php echo $item['kegiatan']; ?> </td>
                    <td> <?php echo $item['tempat']; ?> </td>
                    <td> <?php echo $item['kategori']; ?> </td>
                    <td> <?php echo $item['materi']; ?> </td>
                    <td> <a href="<?php echo base_url(); ?>jadwal/details/<?php echo $item['id'];  ?>"  class="btn btn-block btn-info btn sm"> Details </a> </td>
                </tr>
              <?php } } ?>
                </tbody>
              </table>

            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.0
    </div>
    <strong>Copyright &copy; 2019 <a href="http://bpsdm.kemenkumham.go.id">BPSDM Hukum dan HAM</a>.</strong> All rights reserved.
  </footer>

</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="<?php echo base_url();?>assets/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url();?>assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="<?php echo base_url();?>assets/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url();?>assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="<?php echo base_url();?>assets/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?php echo base_url();?>assets/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url();?>assets/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url();?>assets/dist/js/demo.js"></script>
<!-- page script -->
<script>
  $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>
</body>
</html>
