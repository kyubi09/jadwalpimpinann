<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>Jadwal Pimpinan | Dashboard</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Bootstrap 3.3.4 -->
    <link href="<?php echo base_url();?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- FontAwesome 4.3.0 -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Ionicons 2.0.0 -->
    <link href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="<?php echo base_url();?>assets/dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link href="<?php echo base_url();?>assets/dist/css/skins/_all-skins.min.css" rel="stylesheet" type="text/css" />
    <!-- iCheck -->
    <link href="<?php echo base_url();?>assets/plugins/iCheck/flat/blue.css" rel="stylesheet" type="text/css" />
    <!-- Morris chart -->
    <link href="<?php echo base_url();?>assets/plugins/morris/morris.css" rel="stylesheet" type="text/css" />
    <!-- jvectormap -->
    <link href="<?php echo base_url();?>assets/plugins/jvectormap/jquery-jvectormap-1.2.2.css" rel="stylesheet" type="text/css" />
    <!-- Date Picker -->
    <link href="<?php echo base_url();?>assets/plugins/iCheck/all.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url();?>assets/plugins/datepicker/datepicker3.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url();?>assets/plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css" />
      <link rel="stylesheet" href="<?php echo base_url();?>assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
    <!-- Daterange picker -->
    <link href="<?php echo base_url();?>assets/plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css" />
    <!-- bootstrap wysihtml5 - text editor -->
    <link href="<?php echo base_url();?>assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css" rel="stylesheet" type="text/css" />

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="skin-blue sidebar-mini">
    <div class="wrapper">

      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar ">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">
              <img src="<?php echo base_url();?>assets/dist/img/bpsdm.png" class="img-circle" alt="User Image" />
            </div>
            <div class="pull-left info">
              <p><?php echo mb_strtoupper($this->session->userdata('user')); ?>
              </p>
              <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
          </div>

          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <li class="header">Jadwal</li>
            <li ><a href="<?php echo base_url();?>jadwal"><i class="fa fa-circle-o"></i> Jadwal Terpadu</a></li>
            <?php $user=$this->session->userdata('user');
                  if ($user== "tukabadan") { $pimpinan="Kepala BPSDM"; }
            			if ($user=="tuses") { $pimpinan="Sekretaris BPSDM"; }
            			if ($user=="tufungham") { $pimpinan="Kepala PusFungHAM"; }
            			if ($user=="tupenkom") { $pimpinan="Kepala PusPenKOm"; }
            			if ($user=="tutekpim") { $pimpinan="Kepala PusTekPim"; }
            			if ($user=="tupoltekip") { $pimpinan="Direktur Poltekip"; }
            			if ($user=="tupoltekim") { $pimpinan="Direktur Poltekim"; } ?>

              <?php if ($this->session->userdata('userkode')<4) { ?>
            <li><a href="<?php echo base_url();?>jadwal/pimpinan"><i class="fa fa-clock-o"></i> <?php echo $pimpinan; ?></a></li>
            <li><a href="<?php echo base_url();?>jadwal/search"><i class="fa fa-clock-o"></i> Cari Jadwal</a></li> <?php }
				
			else {
					if ($this->session->userdata('userkode')==4) {?> <li><a href="<?php echo base_url();?>jadwal/gantipass"><i class="fa fa-clock-o"></i> Ganti Password TU</a></li> <?php }
			} ?>
            <li><a href="<?php echo base_url();?>jadwal/password"><i class="fa fa-clock-o"></i> ubah Password</a></li>
            <li><a href="<?php echo base_url();?>login/logout"><i class="fa fa-clock-o"></i> Logout</a></li>
            </ul>
        </section>
        <!-- /.sidebar -->
      </aside>
