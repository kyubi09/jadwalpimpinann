      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Dashboard
            <small>Disposisi</small>
          </h1>
         </section>

        <section class="content">
          <div class="row">
            <!-- left column -->
            <div class="col-md-12">
              <!-- general form elements -->
              <div class="box box-primary">
                <!-- form start -->
                <h3>
                  Disposisi
                </h3>
                <form role="form" action="#" method="post">
                  <div class="box-body">

                    <div class="form-group col-xs-2">
                      <button class="btn btn-info col-xs-12"> Disposisi </button>
                    </div>
                    <div class="form-group col-xs-2">
                        <div class="radio">
                          <label>
                            <input type="radio" name="hadir" id="optionsRadios1" value="1" checked>
                            Hadir
                          </label>
                        </div>
                        <div class="radio">
                          <label>
                            <input type="radio" name="hadir" id="optionsRadios2" value="10">
                            Tidak Hadir
                            </label>
                        </div>
                        <div class="radio">
                          <label>
                            <input type="radio" name="hadir" id="optionsRadios3" value="4">
                            Tentative
                          </label>
                        </div>
                      </div>

                      <div class="form-group col-xs-8">
                          <textarea class="form-control" name="disposisi"rows="3" placeholder="Isi Disposisi"></textarea>
                        </div>


                        <div class="form-group col-xs-2">
                          <button class="btn btn-info col-xs-12"> Didampingi / diwakili </button>
                        </div>

                        <div class="form-group col-xs-10">
                          <div class="checkbox">
                            <label class="col-xs-3"> <input name='ses' type="checkbox"/> Sekretaris BPSDM </label>
                            <label class="col-xs-3"> <input name='tekpim' type="checkbox"/> Ka. PusTekPim </label>
                            <label class="col-xs-3"> <input name='fungham' type="checkbox"/> Ka. PusFungHam </label>
                            <label class="col-xs-3"> <input name='penkom' type="checkbox"/> Ka. PusPenKom </label>
                            <label class="col-xs-3"> <input name='poltekip' type="checkbox"/> Dir. Poltekip </label>
                            <label class="col-xs-3"> <input name='poltekim' type="checkbox"/> Dir. Poltekim </label>
                            <input type="text" name='lain' class="col-xs-6" placeholder="Lainnya"/>
                          </div>
                        </div>

                  </div><!-- /.box-body -->
                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
                </form>
              </div><!-- /.box -->


            </div><!--/.col (left) -->
          </div>   <!-- /.row -->
        </section><!-- /.content -->

        <!-- Main content -->
      </div><!-- /.content-wrapper -->
