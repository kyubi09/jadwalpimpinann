      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Dashboard
            <small>Control panel</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
          </ol>
        </section>

        <section class="content">
                  <div class="row ">
                    <div class="col-xs-12 ">
                      <div class="box bg-gray disabled color-palette">
                        <div class="box-header">
                          <h3 class="box-title">Jadwal Kegiatan Jabatan Pimpinan</h3>
                        </div><!-- /.box-header -->
                        <div class="btn-gorup">
                          <a href="<?php echo base_url(); ?>jadwal/pimpinan" class="btn btn-info"> All Agenda </a>
                          <!--<a href="<?php echo base_url(); ?>jadwal/dihadiri" class="btn btn-info"> Agenda Yang Dihadiri</a> -->
                          <a href="<?php echo base_url(); ?>jadwal/cetak" class="btn btn-info"> Cetak </a>
                          <a href="<?php echo base_url(); ?>jadwal/tambah" class="btn btn-info pull-right"> + Tambah </a>
                        </div>
                        <div class="box-body">
                          <div class="form-group">



                            <div class="input-group date col-xs-3" data-date-format="dd.mm.yyyy">
                              <input  type="text" class="form-control" placeholder="dd.mm.yyyy">
                              <div class="input-group-addon" >
                                <span class="glyphicon glyphicon-th"></span>
                              </div>
                            </div>

                            <div class="input-group date col-xs-3" data-date-format="dd.mm.yyyy">
                              <input  type="text" class="form-control" placeholder="dd.mm.yyyy">
                              <div class="input-group-addon" >
                                <span class="glyphicon glyphicon-th"></span>
                              </div>
                            </div>


                                <!-- /.form group -->
                        </div><!-- /.box-body -->
                      </div><!-- /.box -->
                    </div>
                  </div>
                </section>



        <!-- Main content -->
      </div><!-- /.content-wrapper -->
