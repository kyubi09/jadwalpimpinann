<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class jadwal extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

		 public function __construct()
		 	{
			 parent::__construct();
			 $this->load->model('jadwal_model');
			 if(!$this->session->userdata('is_logged_in'))
				{
					redirect('login');
				}
			}

		public function index()
		{
			 $date=date('Y-m-d',time()+( 1 - date('w'))*24*3600);
			//$data['jadwal']=$this->jadwal_model->terpadu($senin);
			$data['senin']=$date;

			$date=date_create($date);
			date_add($date, date_interval_create_from_date_string('1 days'));
			$data['selasa']=date_format($date, 'Y-m-d');

			date_add($date, date_interval_create_from_date_string('1 days'));
			$data['rabu']=date_format($date, 'Y-m-d');

			date_add($date, date_interval_create_from_date_string('1 days'));
			$data['kamis']=date_format($date, 'Y-m-d');

			date_add($date, date_interval_create_from_date_string('1 days'));
			$data['jumat']=date_format($date, 'Y-m-d');

			date_add($date, date_interval_create_from_date_string('1 days'));
			$data['sabtu']=date_format($date, 'Y-m-d');

			date_add($date, date_interval_create_from_date_string('1 days'));
			$data['minggu']=date_format($date, 'Y-m-d');
			$data['jadwalsenin']['kabadan']=$this->jadwal_model->terpadukabadan($data['senin']);
			$data['jadwalselasa']['kabadan']=$this->jadwal_model->terpadukabadan($data['selasa']);
			$data['jadwalrabu']['kabadan']=$this->jadwal_model->terpadukabadan($data['rabu']);
			$data['jadwalkamis']['kabadan']=$this->jadwal_model->terpadukabadan($data['kamis']);
			$data['jadwaljumat']['kabadan']=$this->jadwal_model->terpadukabadan($data['jumat']);
			$data['jadwalsabtu']['kabadan']=$this->jadwal_model->terpadukabadan($data['sabtu']);
			$data['jadwalminggu']['kabadan']=$this->jadwal_model->terpadukabadan($data['minggu']);
			$data['jadwalsenin']['ses']=$this->jadwal_model->terpaduses($data['senin']);
			$data['jadwalselasa']['ses']=$this->jadwal_model->terpaduses($data['selasa']);
			$data['jadwalrabu']['ses']=$this->jadwal_model->terpaduses($data['rabu']);
			$data['jadwalkamis']['ses']=$this->jadwal_model->terpaduses($data['kamis']);
			$data['jadwaljumat']['ses']=$this->jadwal_model->terpaduses($data['jumat']);
			$data['jadwalsabtu']['ses']=$this->jadwal_model->terpaduses($data['sabtu']);
			$data['jadwalminggu']['ses']=$this->jadwal_model->terpaduses($data['minggu']);
			$data['jadwalsenin']['fungham']=$this->jadwal_model->terpadufungham($data['senin']);
			$data['jadwalselasa']['fungham']=$this->jadwal_model->terpadufungham($data['selasa']);
			$data['jadwalrabu']['fungham']=$this->jadwal_model->terpadufungham($data['rabu']);
			$data['jadwalkamis']['fungham']=$this->jadwal_model->terpadufungham($data['kamis']);
			$data['jadwaljumat']['fungham']=$this->jadwal_model->terpadufungham($data['jumat']);
			$data['jadwalsabtu']['fungham']=$this->jadwal_model->terpadufungham($data['sabtu']);
			$data['jadwalminggu']['fungham']=$this->jadwal_model->terpadufungham($data['minggu']);
			$data['jadwalsenin']['penkom']=$this->jadwal_model->terpadupenkom($data['senin']);
			$data['jadwalselasa']['penkom']=$this->jadwal_model->terpadupenkom($data['selasa']);
			$data['jadwalrabu']['penkom']=$this->jadwal_model->terpadupenkom($data['rabu']);
			$data['jadwalkamis']['penkom']=$this->jadwal_model->terpadupenkom($data['kamis']);
			$data['jadwaljumat']['penkom']=$this->jadwal_model->terpadupenkom($data['jumat']);
			$data['jadwalsabtu']['penkom']=$this->jadwal_model->terpadupenkom($data['sabtu']);
			$data['jadwalminggu']['penkom']=$this->jadwal_model->terpadupenkom($data['minggu']);
			$data['jadwalsenin']['tekpim']=$this->jadwal_model->terpadutekpim($data['senin']);
			$data['jadwalselasa']['tekpim']=$this->jadwal_model->terpadutekpim($data['selasa']);
			$data['jadwalrabu']['tekpim']=$this->jadwal_model->terpadutekpim($data['rabu']);
			$data['jadwalkamis']['tekpim']=$this->jadwal_model->terpadutekpim($data['kamis']);
			$data['jadwaljumat']['tekpim']=$this->jadwal_model->terpadutekpim($data['jumat']);
			$data['jadwalsabtu']['tekpim']=$this->jadwal_model->terpadutekpim($data['sabtu']);
			$data['jadwalminggu']['tekpim']=$this->jadwal_model->terpadutekpim($data['minggu']);
			$data['jadwalsenin']['poltekip']=$this->jadwal_model->terpadupoltekip($data['senin']);
			$data['jadwalselasa']['poltekip']=$this->jadwal_model->terpadupoltekip($data['selasa']);
			$data['jadwalrabu']['poltekip']=$this->jadwal_model->terpadupoltekip($data['rabu']);
			$data['jadwalkamis']['poltekip']=$this->jadwal_model->terpadupoltekip($data['kamis']);
			$data['jadwaljumat']['poltekip']=$this->jadwal_model->terpadupoltekip($data['jumat']);
			$data['jadwalsabtu']['poltekip']=$this->jadwal_model->terpadupoltekip($data['sabtu']);
			$data['jadwalminggu']['poltekip']=$this->jadwal_model->terpadupoltekip($data['minggu']);
			$data['jadwalsenin']['poltekim']=$this->jadwal_model->terpadupoltekim($data['senin']);
			$data['jadwalselasa']['poltekim']=$this->jadwal_model->terpadupoltekim($data['selasa']);
			$data['jadwalrabu']['poltekim']=$this->jadwal_model->terpadupoltekim($data['rabu']);
			$data['jadwalkamis']['poltekim']=$this->jadwal_model->terpadupoltekim($data['kamis']);
			$data['jadwaljumat']['poltekim']=$this->jadwal_model->terpadupoltekim($data['jumat']);
			$data['jadwalsabtu']['poltekim']=$this->jadwal_model->terpadupoltekim($data['sabtu']);
			$data['jadwalminggu']['poltekim']=$this->jadwal_model->terpadupoltekim($data['minggu']);

			//$data['jadwalsenin']['ses']=$this->jadwal_model->terpadu($senin,$ses);
			//$data['jadwalsenin']['fungham']=$this->jadwal_model->terpadu($senin,$fungham);
			//$data['jadwalsenin']['penkom']=$this->jadwal_model->terpadu($senin,$penkom);
			//$data['jadwalsenin']['tekpim']=$this->jadwal_model->terpadu($senin,$tekpim);
			//$data['jadwalsenin']['poltekip']=$this->jadwal_model->terpadu($senin,$poltekip);
			//$data['jadwalsenin']['poltekim']=$this->jadwal_model->terpadu($senin,$poltekim);
			$this->load->view('header');
			$this->load->view('jadwalterpadu',$data);
			$this->load->view('footer');
		}
		public function pimpinan()
		{
			$date=date('Y-m-d',time()+( 1 - date('w'))*24*3600);
		 //$data['jadwal']=$this->jadwal_model->terpadu($senin);
		 $data['tanggal']['senin']=$date;

		 $date=date_create($date);
		 date_add($date, date_interval_create_from_date_string('1 days'));
		 $data['tanggal']['selasa']=date_format($date, 'Y-m-d');

		 date_add($date, date_interval_create_from_date_string('1 days'));
		 $data['tanggal']['rabu']=date_format($date, 'Y-m-d');

		 date_add($date, date_interval_create_from_date_string('1 days'));
		 $data['tanggal']['kamis']=date_format($date, 'Y-m-d');

		 date_add($date, date_interval_create_from_date_string('1 days'));
		 $data['tanggal']['jumat']=date_format($date, 'Y-m-d');

		 date_add($date, date_interval_create_from_date_string('1 days'));
		 $data['tanggal']['sabtu']=date_format($date, 'Y-m-d');

		 date_add($date, date_interval_create_from_date_string('1 days'));
		 $data['tanggal']['minggu']=date_format($date, 'Y-m-d');

			$user=$this->session->userdata('user');
			//echo $user;
			$awal=$data['tanggal']['senin'];
			$akhir=$data['tanggal']['minggu'];
			if ($user== "tukabadan") { $data['jadwal']=$this->jadwal_model->privatekabadan($awal,$akhir); }
			if ($user=="tuses") { $data['jadwal']=$this->jadwal_model->privateses($awal,$akhir); }
			if ($user=="tufungham") { $data['jadwal']=$this->jadwal_model->privatefungham($awal,$akhir); }
			if ($user=="tupenkom") { $data['jadwal']=$this->jadwal_model->privatepenkom($awal,$akhir); }
			if ($user=="tutekpim") { $data['jadwal']=$this->jadwal_model->privatetekpim($awal,$akhir); }
			if ($user=="tupoltekip") { $data['jadwal']=$this->jadwal_model->privatepoltekip($awal,$akhir); }
			if ($user=="tupoltekim") { $data['jadwal']=$this->jadwal_model->privatepoltekim($awal,$akhir); }
			$this->load->view('header');
			$this->load->view('jadwalpimpinan',$data);
			$this->load->view('footer');
		}

		public function cetak()
		{
			$this->load->view('header');
			$this->load->view('cetak');
			$this->load->view('footer');
		}

		public function search()
		{
			if ($this->session->userdata('userkode')<4) {
			//$this->load->view('header');
			$user=$this->session->userdata('user');
			if ($user== "tukabadan") { $data['jadwal']=$this->jadwal_model->searchkabadan(); }
			if ($user== "tuses") { $data['jadwal']=$this->jadwal_model->searchses(); }
			if ($user== "tufungham") { $data['jadwal']=$this->jadwal_model->searchfungham(); }
			if ($user== "tupenkom") { $data['jadwal']=$this->jadwal_model->searchpenkom(); }
			if ($user== "tutekpim") { $data['jadwal']=$this->jadwal_model->searchtekpim(); }
			if ($user== "tupoltekip") { $data['jadwal']=$this->jadwal_model->searchpoltekip(); }
			if ($user== "tupoltekim") { $data['jadwal']=$this->jadwal_model->searchpoltekim(); }
			//echo var_dump($data['jadwal']);
			$this->load->view('datatable',$data);
			}
			else { redirect();}//$this->load->view('footer');
		}

		public function tambah()
		{
			$data['form']=$this->input->post();
			if ($data['form'])
			{
								if ($_FILES['dokumen']['error'] <> 4)
								{
									$tanggal=$this->input->post('tanggal');
									$waktu=$this->input->post('waktu');
									$kegiatan=$this->input->post('kegiatan');
									$tempat=$this->input->post('tempat');
									$materi=$this->input->post('materi');
									$kabadan=$this->input->post('kabadan');
									$ses=$this->input->post('ses');
									$tekpim=$this->input->post('tekpim');
									$fungham=$this->input->post('fungham');
									$penkom=$this->input->post('penkom');
									$poltekip=$this->input->post('poltekip');
									$poltekim=$this->input->post('poltekim');
									$lain=$this->input->post('lain');
									$cp=$this->input->post('cp');
									$dresscode=$this->input->post('dresscode');
									$kategori=$this->input->post('kategori');
									$keterangan=$this->input->post('keterangan');
									$dibuat=$this->session->userdata('user');
									if ($kabadan=='on')
									{
										$kabadan=1;
									}
									else {
										if ($this->session->userdata('user')=='tukabadan' AND !$ses AND !$fungham AND !$penkom AND !$tekpim AND !$poltekip AND !$poltekip  )
										{
											$kabadan=0;
										}
									}
									if ($ses=='on')
									{
										$ses=1;
									}
									if ($fungham=='on')
									{
										$fungham=1;
									}
									if ($penkom=='on')
									{
										$penkom=1;
									}
									if ($tekpim=='on')
									{
										$tekpim=1;
									}
									if ($poltekip=='on')
									{
										$poltekip=1;
									}
									if ($poltekim=='on')
									{
										$poltekim=1;
									}
									if ($this->session->userdata('user')=='tukabadan') {
										$perintahkabadan=1;
									}
									else {
										$perintahkabadan=0;
									}

									//echo var_dump($_FILES);

												//upload files
														$config['upload_path']   = './uploads/';
					     							$config['allowed_types'] = 'png|pdf|ai|xls|ppt|pptx|rar|bmp|jpg|jpeg|png|txt|text|doc|docx|xlsx|';
							 							$config['encrypt_name'] = true;
					     							//$this->load->library('upload', $config);
														$this->load->library('upload',$config);
													if ( !$this->upload->do_upload('dokumen'))
														{
															//echo var_dump($this->input->post());
															//echo "gagal";
															$this->upload->display_errors();
															echo "<br>";
															echo var_dump($this->upload->data());
														}
														else {
									 						$upload_data=$this->upload->data();
															//echo var_dump($this->upload->data());
															echo "sukses";
															$file=$upload_data['file_name'];

															$data['forminput'] = array(
															'tanggal' => $tanggal,
															'waktu' => $waktu,
															'kegiatan' => $kegiatan,
															'tempat' => $tempat,
															'materi' => $materi,
															'kabadan' => $kabadan,
															'ses' => $kabadan,
															'ses' => $ses,
															'tekpim' => $tekpim,
															'fungham' => $fungham,
															'penkom' => $penkom,
															'poltekip' => $poltekip,
															'poltekim' => $poltekim,
															//'lain' => $lain,
															'dresscode' => $dresscode,
															'cp' => $cp,
															'kategori' => $kategori,
															'keterangan' => $keterangan,
															'dibuat' => $dibuat,
															'file' => $file,
															'perintahkabadan' => $perintahkabadan
																);
															$user=$this->session->userdata('user');
															if ($user='tukabadan' || $user='tuses')
															{
																$data['forminput']['lainses']=$lain;
															}
															if ($user='tufungham')
															{
																$data['forminput']['lainfungham']=$lain;
															}
															if ($user='tupenkom')
															{
																$data['forminput']['lainpenkom']=$lain;
															}
															if ($user='tutekpim')
															{
																$data['forminput']['laintekpim']=$lain;
															}
															if ($user='tupoltekip')
															{
																$data['forminput']['lainpoltekip']=$lain;
															}
															if ($user='tupoltekim')
															{
																$data['forminput']['lainpoltekim']=$lain;
															}
														$result=$this->jadwal_model->tambah($data['forminput']);
														if ($result)
														{ echo 'Data jadwal Telah Berhasil Ditambahkan'; }
														else
														{ echo 'gagal'; }
														redirect('');
														}
								}

								if ($_FILES['dokumen']['error'] == 4)

								{
									$tanggal=$this->input->post('tanggal');
									$waktu=$this->input->post('waktu');
									$kegiatan=$this->input->post('kegiatan');
									$tempat=$this->input->post('tempat');
									$materi=$this->input->post('materi');
									$kabadan=$this->input->post('kabadan');
									$ses=$this->input->post('ses');
									$tekpim=$this->input->post('tekpim');
									$fungham=$this->input->post('fungham');
									$penkom=$this->input->post('penkom');
									$poltekip=$this->input->post('poltekip');
									$poltekim=$this->input->post('poltekim');
									$lain=$this->input->post('lain');
									$cp=$this->input->post('cp');
									$dresscode=$this->input->post('dresscode');
									$kategori=$this->input->post('kategori');
									$keterangan=$this->input->post('keterangan');
									$dibuat=$this->session->userdata('user');
									if ($kabadan=='on')
									{
										$kabadan=1;
									}
									else {
										if ($this->session->userdata('user')=='tukabadan' AND !$ses AND !$fungham AND !$penkom AND !$tekpim AND !$poltekip AND !$poltekip  )
										{
											$kabadan=0;
										}
									}
									if ($ses=='on')
									{
										$ses=1;
									}
									if ($fungham=='on')
									{
										$fungham=1;
									}
									if ($penkom=='on')
									{
										$penkom=1;
									}
									if ($tekpim=='on')
									{
										$tekpim=1;
									}
									if ($poltekip=='on')
									{
										$poltekip=1;
									}
									if ($poltekim=='on')
									{
										$poltekim=1;
									}
									if ($this->session->userdata('user')=='tukabadan') {
										$perintahkabadan=1;
									}
									else {
										$perintahkabadan=0;
									}


															$file=NULL;

															$data['forminput'] = array(
															'tanggal' => $tanggal,
															'waktu' => $waktu,
															'kegiatan' => $kegiatan,
															'tempat' => $tempat,
															'materi' => $materi,
															'kabadan' => $kabadan,
															'ses' => $kabadan,
															'ses' => $ses,
															'tekpim' => $tekpim,
															'fungham' => $fungham,
															'penkom' => $penkom,
															'poltekip' => $poltekip,
															'poltekim' => $poltekim,
															'cp' => $cp,
															'dresscode' => $dresscode,
															'kategori' => $kategori,
															'keterangan' => $keterangan,
															'dibuat' => $dibuat,
															'file' => $file,
															'perintahkabadan' => $perintahkabadan
																);
																$user=$this->session->userdata('user');
																if ($user='tukabadan' || $user='tuses')
																{
																	$data['forminput']['lainses']=$lain;
																}
																if ($user='tufungham')
																{
																	$data['forminput']['lainfungham']=$lain;
																}
																if ($user='tupenkom')
																{
																	$data['forminput']['lainpenkom']=$lain;
																}
																if ($user='tutekpim')
																{
																	$data['forminput']['laintekpim']=$lain;
																}
																if ($user='tupoltekip')
																{
																	$data['forminput']['lainpoltekip']=$lain;
																}
																if ($user='tupoltekim')
																{
																	$data['forminput']['lainpoltekim']=$lain;
																}
														$result=$this->jadwal_model->tambah($data['forminput']);
														if ($result)
														{ echo 'Data jadwal Telah Berhasil Ditambahkan'; }
														else
														{ echo 'gagal'; }
														redirect('');
														}
								}



			else {
			$this->load->view('header');
			$this->load->view('tambah');
			$this->load->view('footer');
						}
		}

		public function disposisi($id)
		{

			$data['form']=$this->input->post();
			if ($data['form'])
			{
			$kabadan=$this->input->post('hadir');
			$ses=$this->input->post('ses');
			$fungham=$this->input->post('fungham');
			$penkom=$this->input->post('penkom');
			$tekpim=$this->input->post('tekpim');
			$poltekip=$this->input->post('poltekip');
			$poltekim=$this->input->post('poltekim');
			$lain=$this->input->post('lain');
			$disposisi=$this->input->post('disposisi');

					if ($ses=='on')
					{
						$ses=1;
					}
					if ($fungham=='on')
					{
						$fungham=1;
					}
					if ($penkom=='on')
					{
						$penkom=1;
					}
					if ($tekpim=='on')
					{
						$tekpim=1;
					}
					if ($poltekip=='on')
					{
						$poltekip=1;
					}
					if ($poltekim=='on')
					{
						$poltekim=1;
					}

					$data['forminput'] = array(
					'ses' => $ses,
					'fungham' => $fungham,
					'penkom' => $penkom,
					'tekpim' => $tekpim,
					'poltekip' => $poltekip,
					'poltekim' => $poltekim,
					'kabadan' => $kabadan,
					'disposisi' => $disposisi,
					'lain' => $lain
						);

					$result=$this->jadwal_model->update($data['forminput'],$id);
					//	if ($result)
						{ echo 'Data jadwal Telah Berhasil Ditambahkan'; }
					//	else
					//	{ echo 'gagal'; }
					redirect('');

			}

			$this->load->view('header');
			$this->load->view('disposisi');
			$this->load->view('footer');
		}

		public function hadir()
		{
			$data['form']=$this->input->post();
			if ($data['form'])
			{
				$hadir=$this->input->post('hadir');
				$id=$this->input->post('id');
				$lain=$this->input->post('lain');
				if($this->session->userdata('user')=='tuses')
				{
				$data['forminput'] = array(
				'ses' => $hadir,
				'lainses' => $lain
					);
				}
				if($this->session->userdata('user')=='tukabadan')
				{
				$data['forminput'] = array(
				'kabadan' => $hadir,
				'lainses' => $lain
					);
				}
				if($this->session->userdata('user')=='tufungham')
				{
				$data['forminput'] = array(
				'fungham' => $hadir,
				'lainfungham' => $lain
					);
				}

				if($this->session->userdata('user')=='tupenkom')
				{
				$data['forminput'] = array(
				'penkom' => $hadir,
				'lainpenkom' => $lain
					);
				}
				if($this->session->userdata('user')=='tutekpim')
				{
				$data['forminput'] = array(
				'tekpim' => $hadir,
				'laintekpim' => $lain
					);
				}
				if($this->session->userdata('user')=='tupoltekip')
				{
				$data['forminput'] = array(
				'poltekip' => $hadir,
				'lainpoltekip' => $lain
					);
				}

				if($this->session->userdata('user')=='tupoltekim')
				{
				$data['forminput'] = array(
				'poltekim' => $hadir,
				'lainpoltekim' => $lain
					);
				}

				$result=$this->jadwal_model->update($data['forminput'],$id);
				redirect('jadwal/pimpinan');
			}

		}

		public function hapus($id)
		{
			$result=$this->jadwal_model->delete($id);
			redirect('jadwal/pimpinan');
		}

		public function update($id)
		{
			$data['form']=$this->input->post();
			if ($data['form'])
			{
				$tanggal=$this->input->post('tanggal');
				$waktu=$this->input->post('waktu');
				$kegiatan=$this->input->post('kegiatan');
				$tempat=$this->input->post('tempat');
				$materi=$this->input->post('materi');
				$kabadan=$this->input->post('kabadan');
				$ses=$this->input->post('ses');
				$tekpim=$this->input->post('tekpim');
				$fungham=$this->input->post('fungham');
				$penkom=$this->input->post('penkom');
				$poltekip=$this->input->post('poltekip');
				$poltekim=$this->input->post('poltekim');
				$lain=$this->input->post('lain');
				$cp=$this->input->post('cp');
				$dresscode=$this->input->post('dresscode');
				$kategori=$this->input->post('kategori');
				$keterangan=$this->input->post('keterangan');
				echo $kabadan;
				if ($kabadan=='tidak') { $kabadan=NULL; }
				if ($ses=='tidak') { $ses=NULL; }
				if ($fungham=='tidak') { $fungham=NULL; }
				if ($tekpim=='tidak') { $tekpim=NULL; }
				if ($penkom=='tidak') { $penkom=NULL; }
				if ($poltekim=='tidak') { $poltekim=NULL; }
				if ($poltekip=='tidak') { $poltekip=NULL; }
				$data['forminput'] = array(
				'tanggal' => $tanggal,
				'waktu' => $waktu,
				'kegiatan' => $kegiatan,
				'tempat' => $tempat,
				'materi' => $materi,
				'kabadan' => $kabadan,
				'ses' => $ses,
				'tekpim' => $tekpim,
				'fungham' => $fungham,
				'penkom' => $penkom,
				'poltekip' => $poltekip,
				'poltekim' => $poltekim,
				'lain' => $lain,
				'cp' => $cp,
				'dresscode' => $dresscode,
				'kategori' => $kategori,
				'keterangan' => $keterangan
					);
					$result=$this->jadwal_model->update($data['forminput'],$id);
					redirect('jadwal/pimpinan');
				}


			$data=$this->jadwal_model->getbyid($id);
			$data=$data['0'];

			//echo var_dump($data);
			$this->load->view('header');
			$this->load->view('update',$data);
			$this->load->view('footer');
			//redirect('jadwal/pimpinan');
		}

		public function password()
		{
			$data['form']=$this->input->post();
			if ($data['form'])
			{
					$lama=$this->input->post('passwordlama');
					$baru=$this->input->post('password');
					$lama=md5($lama);
					$username=$this->session->userdata('user');
					$cek=$this->jadwal_model->cekpass($lama,$username);
					$baru=md5($baru);
					if($cek)
					{
						$data['forminput'] = array(
						'password' => $baru
							);
						$this->jadwal_model->gantipass($data['forminput'],$username);
						echo "sukses";
						redirect ('login/logout');
					}
					else
					{
							$data['salah']=1;
							$this->load->view('header');
							$this->load->view('password',$data);
							$this->load->view('footer');
					}
			}
			else {
			$data['salah']=0;
			$this->load->view('header');
			$this->load->view('password',$data);
			$this->load->view('footer');
		}
		}

		public function gantipass()
		{
			if ($this->session->userdata('userkode')<4) {
			redirect(); }
			$data['form']=$this->input->post();
			if ($data['form'])
			{
					$username=$this->input->post('user');
					$baru=$this->input->post('password');
					$baru=md5($baru);

						$data['forminput'] = array(
						'password' => $baru
							);
						$this->jadwal_model->gantipass($data['forminput'],$username);
						redirect ('jadwal/gantipass');
				}
			$this->load->view('header');
			$this->load->view('gantipass');
			$this->load->view('footer');
		}

	public function details($id)

	{
		$data['jadwal']=$this->jadwal_model->searchbyid($id);
		$this->load->view('header');
		$this->load->view('jadwalpimpinan',$data);
		$this->load->view('footer');
	}
}
