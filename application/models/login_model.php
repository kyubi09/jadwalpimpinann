<?php

	class login_model extends CI_Model
	{

		public function __construct()
	{
		parent::__construct();

		$this->load->database();
	}

	function validate($username, $password)
	{
  $this->db->where('username', $username);
  $this->db->where('password', $password);
  $query = $this->db->get('user');

  if($query->num_rows() == 1)
  	{
    return true;
  	}
	}

	function getdata($username)
	{
			$this->db->select('*');
			$this->db->where('username', $username);
		  $query = $this->db->get('user');
			return $query->result_array();
	}

}
