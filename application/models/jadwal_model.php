<?php

	class Jadwal_model extends CI_Model
	{

		public function __construct()
	{
		parent::__construct();

		$this->load->database();
	}
  function tambah($data)
	{
  $res = $this->db->insert('jadwal', $data);
	return  $res;
	//return $res;
	}



	function terpadukabadan($tanggal)
	{
		$this->db->select('hadir,kegiatan,tempat,waktu,kabadan');
		$this->db->where('tanggal',$tanggal);
		$this->db->where('kabadan is NOT NULL', NULL, FALSE);
		$query=$this->db->get('jadwal');
		$this->db->order_by('waktu', "asc");
		return $query->result_array();

	}

	function terpaduses($tanggal)
	{
		$this->db->select('hadir,kegiatan,tempat,waktu,ses');
		$this->db->where('tanggal',$tanggal);
		$this->db->where('ses is NOT NULL', NULL, FALSE);
		$this->db->order_by('waktu', "asc");
		$query=$this->db->get('jadwal');
		return $query->result_array();

	}

	function terpadufungham($tanggal)
	{
		$this->db->select('hadir,kegiatan,tempat,waktu,fungham');
		$this->db->where('tanggal',$tanggal);
		$this->db->where('fungham is NOT NULL', NULL, FALSE);
		$this->db->order_by('waktu', "asc");
		$query=$this->db->get('jadwal');
		return $query->result_array();
	}

	function terpadupenkom($tanggal)
	{
		$this->db->select('hadir,kegiatan,tempat,waktu,penkom');
		$this->db->where('tanggal',$tanggal);
		$this->db->where('penkom is NOT NULL', NULL, FALSE);
		$this->db->order_by('waktu', "asc");
		$query=$this->db->get('jadwal');
		return $query->result_array();

	}
	function terpadutekpim($tanggal)
	{
		$this->db->select('hadir,kegiatan,tempat,waktu,tekpim');
		$this->db->where('tanggal',$tanggal);
		$this->db->where('tekpim is NOT NULL', NULL, FALSE);
		$this->db->order_by('waktu', "asc");
		$query=$this->db->get('jadwal');
		return $query->result_array();

	}

	function terpadupoltekip($tanggal)
	{
		$this->db->select('hadir,kegiatan,tempat,waktu,poltekip');
		$this->db->where('tanggal',$tanggal);
		$this->db->where('poltekip is NOT NULL', NULL, FALSE);
		$this->db->order_by('waktu', "asc");
		$query=$this->db->get('jadwal');
		return $query->result_array();

	}

	function terpadupoltekim($tanggal)
	{
		$this->db->select('hadir,kegiatan,tempat,waktu,poltekim');
		$this->db->where('tanggal',$tanggal);
		$this->db->where('poltekim is NOT NULL', NULL, FALSE);
		$this->db->order_by('waktu', "asc");
		$query=$this->db->get('jadwal');
		return $query->result_array();

	}

	function privatekabadan($awal,$akhir)
	{
		$this->db->select('*');
		$this->db->where('tanggal >=',$awal);
		$this->db->where('tanggal <=',$akhir);
		$this->db->where('kabadan is NOT NULL', NULL, FALSE);
		$this->db->or_where('dibuat', "tukabadan");
		$this->db->order_by('tanggal', "asc");
		$this->db->order_by('waktu', "asc");
		$query=$this->db->get('jadwal');
		return $query->result_array();
	}
	function privateses($awal,$akhir)
	{
		$this->db->select('*');
		$this->db->where('tanggal >=',$awal);
		$this->db->where('tanggal <=',$akhir);
		$this->db->where('ses is NOT NULL', NULL, FALSE);
		$this->db->or_where('dibuat', "tuses");
		$this->db->order_by('tanggal', "asc");
		$this->db->order_by('waktu', "asc");
		$query=$this->db->get('jadwal');
		return $query->result_array();
	}

	function privatefungham($awal,$akhir)
	{
		$this->db->select('*');
		$this->db->where('tanggal >=',$awal);
		$this->db->where('tanggal <=',$akhir);
		$this->db->where('fungham is NOT NULL', NULL, FALSE);
		$this->db->or_where('dibuat', "tufungham");
		$this->db->order_by('tanggal', "asc");
		$this->db->order_by('waktu', "asc");
		$query=$this->db->get('jadwal');
		return $query->result_array();
		}
	function privatepenkom($awal,$akhir)
	{
		$this->db->select('*');
		$this->db->where('tanggal >=',$awal);
		$this->db->where('tanggal <=',$akhir);
		$this->db->where('penkom is NOT NULL', NULL, FALSE);
		$this->db->or_where('dibuat', "tupenkom");
		$this->db->order_by('tanggal', "asc");
		$this->db->order_by('waktu', "asc");
		$query=$this->db->get('jadwal');
		return $query->result_array();
	}

	function privatetekpim($awal,$akhir)
	{
		$this->db->select('*');
		$this->db->where('tanggal >=',$awal);
		$this->db->where('tanggal <=',$akhir);
		$this->db->where('tekpim is NOT NULL', NULL, FALSE);
		$this->db->or_where('dibuat', "tutekpim");
		$this->db->order_by('tanggal', "asc");
		$this->db->order_by('waktu', "asc");
		$query=$this->db->get('jadwal');
		return $query->result_array();
		}
	function privatepoltekip($awal,$akhir)
	{
		$this->db->select('*');
		$this->db->where('tanggal >=',$awal);
		$this->db->where('tanggal <=',$akhir);
		$this->db->where('poltekip is NOT NULL', NULL, FALSE);
		$this->db->or_where('dibuat', "tupoltekip");
		$this->db->order_by('tanggal', "asc");
		$this->db->order_by('waktu', "asc");
		$query=$this->db->get('jadwal');
		return $query->result_array();
		}

	function privatepoltekim($awal,$akhir)
	{
		$this->db->select('*');
		$this->db->where('tanggal >=',$awal);
		$this->db->where('tanggal <=',$akhir);
		$this->db->where('poltekim is NOT NULL', NULL, FALSE);
		$this->db->or_where('dibuat', "tupoltekip");
		$this->db->order_by('tanggal', "asc");
		$this->db->order_by('waktu', "asc");
		$query=$this->db->get('jadwal');
		return $query->result_array();
		}

		function update($data,$id)
		{
		$this->db->where('id', $id);
		$res = $this->db->update('jadwal', $data);
		return  $res;
		//return $res;
		}

		function getlain($id)
		{
			$this->db->select('lain');
			$this->db->where('id',$id);
			$query=$this->db->get('jadwal');
			return $query->result_array();

		}
		function getbyid($id)
		{
			$this->db->select('*');
			$this->db->where('id',$id);
			$query=$this->db->get('jadwal');
			return $query->result_array();

		}
		function delete($id)
		{
			$this->db->where('id', $id);
			$this->db->delete('jadwal');
		}

		function cekpass($lama, $username)
		{
	  $this->db->where('username', $username);
	  $this->db->where('password', $lama);
	  $query = $this->db->get('user');

	  if($query->num_rows() == 1)
	  	{
	    return true;
	  	}
		}

		function gantipass($data,$username)
		{
		$this->db->where('username', $username);
		$res = $this->db->update('user', $data);
		return  $res;
		//return $res;
		}

		function searchkabadan()
		{
			$this->db->select('kegiatan,tempat,kategori,materi,id');
			$this->db->where('kabadan is NOT NULL', NULL, FALSE);
			$this->db->or_where('dibuat', "tukabadan");
			$query=$this->db->get('jadwal');
			return $query->result_array();
		}

		function searchses()
		{
			$this->db->select('kegiatan,tempat,kategori,materi,id');
			$this->db->where('ses is NOT NULL', NULL, FALSE);
			$this->db->or_where('dibuat', "tuses");
			$query=$this->db->get('jadwal');
			return $query->result_array();
		}
		function searchfungham()
		{
			$this->db->select('kegiatan,tempat,kategori,materi,id');
			$this->db->where('fungham is NOT NULL', NULL, FALSE);
			$this->db->or_where('dibuat', "tufungham");
			$query=$this->db->get('jadwal');
			return $query->result_array();
		}

		function searchpenkom()
		{
			$this->db->select('kegiatan,tempat,kategori,materi,id');
			$this->db->where('penkom is NOT NULL', NULL, FALSE);
			$this->db->or_where('dibuat', "tupenkom");
			$query=$this->db->get('jadwal');
			return $query->result_array();
		}
		function searchtekpim()
		{
			$this->db->select('kegiatan,tempat,kategori,materi,id');
			$this->db->where('tekpim is NOT NULL', NULL, FALSE);
			$this->db->or_where('dibuat', "tutekpim");
			$query=$this->db->get('jadwal');
			return $query->result_array();
		}

		function searchpoltekip()
		{
			$this->db->select('kegiatan,tempat,kategori,materi,id');
			$this->db->where('poltekip is NOT NULL', NULL, FALSE);
			$this->db->or_where('dibuat', "tupoltekip");
			$query=$this->db->get('jadwal');
			return $query->result_array();
		}

		function searchpoltekim()
		{
			$this->db->select('kegiatan,tempat,kategori,materi,id');
			$this->db->where('poltekim is NOT NULL', NULL, FALSE);
			$this->db->or_where('dibuat', "tupoltekim");
			$query=$this->db->get('jadwal');
			return $query->result_array();
		}

		function searchbyid($id)
		{
			$this->db->select('*');
			$this->db->where('id',$id);
			$query=$this->db->get('jadwal');
			return $query->result_array();
			}

}
